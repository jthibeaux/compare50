$(document).ready(function() {
  $(".multiselect").multiselect();
  enemies();
  view();
  $("#container .highcharts-legend").hide();
  $("select").change(function() {
	enemies();
	view();
	var myClass = $(this).parent('div').parent('div').attr("class");
	$('div.' + myClass + ' div select').not(this).each(function() {
 		$(this).val('');
	});
  });
});

function enemies() {
  if($("select.type").val() == 'by_state') {
  	//hide_this('year');
   	hide_this('sort');
  	show_this('region');
  	//show_this('start_year');
  	//show_this('end_year');
  }
  if($("select.type").val() == 'all') {
  	//show_this('year');
   	show_this('sort');
  	hide_this('region');
  	//hide_this('start_year');
  	//hide_this('end_year');
  }
}

function hide_this(element) {
	$("input." + element).hide();
	$("select." + element).hide();
	$("div." + element).hide();
	$("input." + element).attr('disabled', 'disabled');
	$("select." + element).attr('disabled', 'disabled');
}
	
function show_this(element) {
	$("input." + element).show();
	$("select." + element).show();
	$("div." + element).show();
	$("input." + element).removeAttr('disabled');
	$("select." + element).removeAttr('disabled');
}

//swap item1 for item2
function swap(item1, item2){
	hide_this(item1);
	hide_this(item2);
	$("." + item1).addClass('swap').removeClass(item1);
	$("." + item2).removeClass(item2).addClass(item1);
	$(".swap").addClass(item2).removeClass('swap');
	show_this(item1);
}

jQuery.fn.multiselect = function() {
    $(this).each(function() {
        var checkboxes = $(this).find("input:checkbox");
        checkboxes.each(function() {
            var checkbox = $(this);
            // Highlight pre-selected checkboxes
            if (checkbox.attr("checked"))
                checkbox.parent().addClass("multiselect-on");
 
            // Highlight checkboxes that the user selects
            checkbox.click(function() {
                if (checkbox.attr("checked"))
                    checkbox.parent().addClass("multiselect-on");
                else
                    checkbox.parent().removeClass("multiselect-on");
            });
        });
    });
};
