function view() {
	hide_this('ethnicity');
	hide_this('age');
	hide_this('income');
	hide_this('education');
	hide_this('tenure');
	hide_this('origin');
	show_this($("select.data").val());
	
  if(($("select.data").val() == 'income') && ($("select.income").val() == 'median')) {
    hide_this('values');
  } else {
  	show_this('values');
  }
}