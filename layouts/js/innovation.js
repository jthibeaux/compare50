function view() {
	if (($("select.type").val() == 'by_state') || ($("input.type").val() == 'by_state')) { 
		if($("select.data").val() == 'capital') {
			hide_this('region');
			show_this('venture');
		} else {
			show_this('region');
			hide_this('venture');
		}
	} else {
		hide_this('region');
		hide_this('venture');
	}
}