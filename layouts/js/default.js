$(document).ready(function() {
  var pathname = window.location.pathname;
  var model = pathname.split('/');

  if((model[1] == 'growth') || (model[1] == 'qcew') || (model[1] == 'ces') || (model[1] == 'housing') || (model[1] == 'rgdp') || (model[1] == 'government')) { 
  	var set = 'economy';
  } else if((model[1] == 'layoffs') || (model[1] == 'employment') || (model[1] == 'unemployment') || (model[1] == 'hhemp') || (model[1] == 'hhunemp') || (model[1] == 'qcewreport') || (model[1] == 'disparity')) { 
  	var set = 'jobs';
  } else if((model[1] == 'earnings') || (model[1] == 'poverty') || (model[1] == 'personal')) { 
  	var set = 'income';
  } else if((model[1] == 'population') || (model[1] == 'community')) { 
  	var set = 'demographics';
  } else if(model[1] == 'pages') { 
  	var set = model[2];
  } else {
  	var set = model[1];
  }

  if(set != '') {
	$('#menu-' + set).addClass('active');
  	$('a.sub.' + model[1]).parent('li').addClass('checked');
	if($.trim($('#stats').text()) != '') {
		$('#stats').before('<a id="notes-link"><b class="icon-plus-sign"></b> Technical Notes</a>');
	} 
  	$('#notes-link').click(function() {
  		if($('#notes-link b').attr('class') == 'icon-plus-sign') {
			$('#stats').addClass('stats-show');
			$('#notes-link b').removeClass('icon-plus-sign').addClass('icon-minus-sign');
		} else {
			$('#stats').removeClass('stats-show');
			$('#notes-link b').removeClass('icon-minus-sign').addClass('icon-plus-sign');
		}
	});
	if($.trim($('#about-info').text()) == '') {
		$('#about-info').hide();
		$('#graph-info').width('96%');
	} 
	if($.trim($('#graph-info').text()) == '') {
		$('#graph-info').hide();
		$('#about-info').width('96%');
	} 
	
	$("div.submit").insertAfter("div.inner-menu");
	var newDiv = '<div class="years"></div>';
	$(newDiv).insertBefore($("div.field_e"));
	$("div.years").append($("div.field_e")).append($("div.field_d"));
}
	
	//setMinLiWidth();
});


//Found at http://www.mredkj.com/javascript/nfbasic.html
function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function tickIntervals(value) {
		if ((value >= 1000000000) || (value <= -1000000000)) { // use B abbreviation
			return (value / 1000000000) + 'B';
		} else if ((value >= 1000000) || (value <= -1000000)) { // use M abbreviation
			return (value / 1000000) + 'M';
		} else if ((value >= 1000) || (value <= -1000)) { // use k abbreviation
			return (value / 1000) + 'k';
		} else { //just give the number back
			return num = value;
		} 
}

function disableYears(firstYear, finalYear) {
	for(var year = firstYear; year <= finalYear; year++) {
		$("select.start_year option[value='" + year + "']").attr("disabled", "disabled");
		$("select.end_year option[value='" + year + "']").attr("disabled", "disabled");
	}
	if (($("select.start_year option:selected").val() >= firstYear) && ($("select.start_year option:selected").val() <= finalYear)) {
		$("select.start_year").val(finalYear+1);
	}
	if (($("select.end_year option:selected").val() >= firstYear) && ($("select.end_year option:selected").val() <= finalYear)) {
		$("select.end_year").val(finalYear-1);
	}
}

function enableYears(firstYear, finalYear) {
	for(var year = firstYear; year <= finalYear; year++) {
		$("select.start_year option[value='" + year + "']").removeAttr("disabled");
		$("select.end_year option[value='" + year + "']").removeAttr("disabled");
	}
}

function disableTypeAll() {
	$("select.type option[value='all']").attr("disabled", "disabled");
	$("select.type").val('by_state');
	show_this('region');
}

function enableTypeAll() {
	$("select.type option[value='all']").removeAttr("disabled");
}

function setMinLiWidth() {
        var maxWidth = 0;
        var elemWidth = 0;
        $('li.menu-item').each(function() {
            elemWidth = parseInt($(this).css('width'));
            ulPadding = String($(this).children('ul').css('padding-right'));
	        rightPadding = parseInt(ulPadding);
            ulPadding = String($(this).children('ul').css('padding-left'));
	        leftPadding = parseInt(ulPadding);
			if((leftPadding != undefined) || (rightPadding != undefined)) {
		        $(this).children('ul').each(function() {
		        	var minWidth = (elemWidth - (leftPadding + rightPadding)) + "px";
		        	$(this).css('min-width', minWidth);
		    	});
        	}
        });
}

function layoffYearAdjustment() {
	if($("select.data option:selected").attr('class') == 'layoffs/index') {
		disableYears(1990,1995);
	} else {
		enableYears(1990,1995);
	}
}

function traceback() {
	$("input.traceback").val($("select.data option:selected").attr('class'));
}

function jobsAdjustments() {
	traceback();
	if(($("input.traceback").val() == 'qcewreport/index') && ($("select.data").val() == 'stat')) {
		show_this('industry');
		show_this('set');
	} else {
		hide_this('industry');
		hide_this('set');
	}
	empAdjustments();
}

function empAdjustments() {
	if(($("input.traceback").val() == 'unemployment/index') || ($("input.traceback").val() == 'employment/index')) {
		enableTypeAll();
		show_this('subset');
	} else if ($("input.traceback").val() == 'disparity/index') {
		enableTypeAll();
		hide_this('subset');
	} else {
		disableTypeAll();
		hide_this('subset');
	}
	
	if(($("input.traceback").val() == 'hhemp/index') || ($("input.traceback").val() == 'hhunemp/index')) {
		enableYears(2012,2012);
	} else {
		disableYears(2012,2012);
	}
	traceback();
}

function unempAdjustments() {
	
	if($("input.traceback").val() == 'disparity/index') {
		show_this('demographic');
		if($("select.demographic").val() == 'gender') {
			hide_this('ethnicity1');
		    hide_this('ethnicity2');
		} else {
			show_this('ethnicity1');
			show_this('ethnicity2');
		}
	} else {
		hide_this('ethnicity1');
		hide_this('ethnicity2');
		hide_this('demographic');
	}
	
	empAdjustments();
}

function earningsAdjustments() {
  if ($("select.statistic option:selected").attr('class') != undefined) {
  	$("input.traceback").val($("select.statistic option:selected").attr('class'));
  } else {
	traceback();
  }
  
	if($("select.statistic").val() == 'median_weekly_gap' || $("select.statistic").val() == 'median_hourly_gap') {
		if($("select.demographic").val() == 'all') {
			$("select.demographic").val('gender');
		}
		$("select.demographic option[value='all']").attr("disabled", "disabled");
	} else {
		$("select.demographic option[value='all']").removeAttr("disabled");
	}
	
  if($("select.demographic").val() == 'gender' || $("select.differential").val() == '') {
    hide_this('ethnicity1');
    hide_this('ethnicity2');
    if($("select.statistic").val() == 'median_weekly_gap' || $("select.statistic").val() == 'median_hourly_gap') {
    	hide_this('gender1');
    } else {
    	show_this('gender1');
    }
  }
  if($("select.demographic").val() == 'ethnicity') {
    show_this('ethnicity1');
    hide_this('gender1');
    if($("select.statistic").val() == 'median_weekly_gap' || $("select.statistic").val() == 'median_hourly_gap') {
    	show_this('ethnicity2');
    } else {
    	hide_this('ethnicity2');
    }
  }
  if($("select.demographic").val() == 'all' || $("input.traceback").val() == 'qcewearnings/index') {
    hide_this('ethnicity1');
    hide_this('ethnicity2');
    hide_this('gender1');
  }
}

function qcewearningsFix() {
  if($("input.traceback").val() == 'qcewearnings/index') {
  	$("select.statistic").addClass('data').removeClass('statistic');
  	$("select.data").attr('name', 'field_a[data]');
  	disableTypeAll();
  } else {
  	$("select.data").addClass('statistic').removeClass('data');
  	$("select.statistic").attr('name', 'field_a[statistic]');
  	enableTypeAll();
  }
}