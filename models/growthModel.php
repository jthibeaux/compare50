<?php

class growthModel extends model {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('data');

    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family growth [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family growth for All States in [F|ELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    if (strpos($option, 'growth') || $option = 'gsp_labors_share') {
      set('y-axis', 'Percentage');
      set('data_pre', ' ');
      set('data_post', '%');
    } else {
      set('y-axis', 'Growth (USD)');
      set('data_pre', '$');
      set('data_post', ' ');
    }
  }

  protected function get_details_adv() {  

  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('growth', 'index', 'data', 'real_gsp_percapita_growth', 'type', 'by_state', 'start_year', START_YEAR+1, 'end_year', END_YEAR, 'region', 'US,CA');
    } else {
      $default_request = array('growth', 'advanced', 'data', 'real_gsp_percapita_growth', 'type', 'by_state', 'start_year', START_YEAR+1, 'end_year', END_YEAR, 'region', 'US,CA');
    }
    set ("default_request", $default_request);
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    
    $years_start =  $this->years_option('start');
    $years_end =  $this->years_option('end');
    unset($years_start[0]);
    unset($years_end[0]);
  
    $views_option = array(
      'data' => array(
        array('value' => 'real_gsp_percapita_growth', 'display_name' => 'Per Capita Real GSP Growth (all industries)', 'class' => 'growth/index'),
        array('value' => 'real_gsp_growth', 'display_name' => 'Real GSP Growth (all industries)', 'class' => 'growth/index'),
        array('value' => 'gsp_labors_share', 'display_name' => "Labor's Share of GSP (all industries)", 'class' => 'growth/index'),
        array('value' => 'all', 'display_name' => 'Employment Growth (all industries, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'all_private', 'display_name' => 'Employment Growth (all industries, private, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'manufacturing_all_private', 'display_name' => 'Employment Growth (manufacturing, private, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'tech', 'display_name' => 'Employment Growth (professional, scientific, and technical services, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'total_nonfarm', 'display_name' => 'Non-farm Employment Growth (CES data)', 'class' => 'ces/index'),
        array('value' => 'total_private', 'display_name' => 'Private Employment Growth (CES data)', 'class' => 'ces/index'),
        array('value' => 'wage_all', 'display_name' => 'Wage Growth (all industries)', 'class' => 'qcew/index'),
        array('value' => 'wage_all_private', 'display_name' => 'Wage Growth (all industries, private)', 'class' => 'qcew/index'),
        array('value' => 'wage_manufacturing_all_private', 'display_name' => 'Wage Growth (manufacturing, private)', 'class' => 'qcew/index'),
        //'element_name' => 'Series',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'type' => $this->type_option(),
      'start_year' => $years_start,
      'end_year' => $years_end,
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }

  protected function region_option() {
    $option = parent::region_option();
    unset($option[54]);
    unset($option[52]);
    
    return $option;
  }
  
  public function query_all($query_array) {  
    $stat = chart_info('data');
    if($stat == 'gsp_labors_share' && $query_array['conditions']['year'][1] == 2011) {
      $query_array['conditions']['year'][1] = 2010;
    }
    $data = growthModel::query_format($query_array);
    $finished = array();
    $start_year = $query_array['conditions']['year'][0];
    $end_year = $query_array['conditions']['year'][1];

    if($stat == 'gsp_labors_share') {
      foreach($data as $state => $year_data) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year) * 100;
      }
    } else {
      $deviation = growthModel::standard_deviation($query_array);
      foreach($data as $state => $years) {
        if(count($years) === 2 && is_state($state) !== false) {
          $finished[$state] = (pow((1 + $deviation[$state]), ($end_year - $start_year)) - 1) * 100;
        }
      }
    }
    asort($finished);
    return growthModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    if(chart_info('data') == 'gsp_labors_share' && $query_array['conditions']['year'][1] == 2011) {
      $query_array['conditions']['year'][1] = 2010;
    }
    $results = growthModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }

    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return growthModel::format_JSON_by_state($results);
  }
  
  protected function get_options_adv() {

  }

  protected function standard_deviation($query_array) { //μ
    $data = growthModel::query_format($query_array, true);
    $deviants = array();
    $field = chart_info('data');
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    foreach($data as $state => $year_data) {
      $deviation = 0;
      foreach($year_data as $stat) {
        $deviation += $stat[$field];
      }
      $deviants[$state] = ($deviation/  ($end_year - $start_year)); 
    }
    return $deviants;
  }

}

?>
