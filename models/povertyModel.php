<?php

class povertyModel extends model {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('rate');

    //Format bits of the title
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [F|ELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    if ($type == 'by_state') {
      set('y-axis', 'Poverty Rate');
    } else {
      set('y-axis', 'Percentage Point Change');
    }
    set('data_pre', '');
    set('data_post', '%');
  }

  protected function get_details_adv() {  

  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('poverty', 'index', 'rate', 'poverty_rate', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    } else {
      $default_request = array('poverty', 'index', 'rate', 'poverty_rate', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    }
    set ("default_request", $default_request);
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
  
    $views_option = array(
      'rate' => array(
        array('value' => 'poverty_rate', 'display_name' => 'Poverty Rate'),
        array('value' => 'poverty_rate_adj', 'display_name' => 'Poverty Rate, Adjusted'),
        array('value' => 'age_poverty_rate', 'display_name' => 'Poverty Rates, Aged 25-64'),
        array('value' => 'age_poverty_rate_adj', 'display_name' => 'Alternative Poverty Rates, Aged 25-64'),
        //'element_name' => 'Poverty',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      //'option' => array('value' => 'adj', 'type'=>'hidden', 'placement' => 'field_b'),
      'type' => $this->type_option(),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      //'year' => $this->years_option(),
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_options_adv() {

  }
  
  public function query_all($query_array) {
    $data = povertyModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year'); 
    $stat = str_replace('age_', '', chart_info('rate'));

    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year) * 100;
       }
    }
    
    asort($finished);
    return povertyModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = povertyModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return povertyModel::format_JSON_by_state($results);
  }

}

?>
