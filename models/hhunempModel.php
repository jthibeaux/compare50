<?php

class hhunempModel extends model2 {

  public function __construct() {
    $request = $GLOBALS['request'];
    $default = $this->get_defaults();
    $data = chart_info('data', $request);
    
    if(!$data) {
      $data = chart_info('data', $default);
    }

    $start_year = query_db(array('table' => 'household', 'select' => 'MIN(year)', 'conditions' => array('data' => $data)));
    $end_year = query_db(array('table' => 'household', 'select' => 'MAX(year)', 'conditions' => array('data' => $data)));
    $this->start_year = array_pop(array_pop($start_year));
    $this->end_year = array_pop(array_pop($end_year));
    $default[] = 'start_year';
    $default[] = $this->start_year;
    $default[] = 'end_year';
    $default[] = $this->end_year;
    
    set('start_year', $this->start_year);
    set('end_year', $this->end_year);
    set ("default_request", $default);
    $this->get_options();
    $this->get_details();
  }

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $data = chart_info('data');

    //Format bits of the title
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(!isset($title['field_e']) || $title['field_e'] == '') {
      $title['field_e'] = $GLOBALS['start_year'];
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' for ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_a'] . ' for ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    set('y-axis', $title['field_a']);
    set('data_pre', ' ');
    set('data_post', ' ');
  }
  
  protected function get_defaults() {
    $default_request = array('hhunemp', 'index', 'data', 'unemp', 'type', 'by_state', 'region', 'CA,NY,TX');
    set ("default_request", $default_request);
    return $default_request;
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region['class'] = 'regions';
    
    $start_years = $this->years_option('start');
    $end_years = $this->years_option('end');
    $start_years[] = $end_years[] = array('value' => 2012, 'display_name' => 2012);
  
    $views_option = array(
      'data' => array(
        array('value' => 'unemployment', 'display_name' => 'Unemployment Rate', 'class' => 'unemployment/index'),
        array('value' => 'unemp', 'display_name' => 'Household Unemployment', 'class' => 'hhunemp/index'),
        array('value' => 'nilf_disc', 'display_name' => '% Who Are Discouraged', 'class' => 'unemployment/index'),
        array('value' => 'mean_unemp', 'display_name' => 'Mean Unemployment Duration', 'class' => 'unemployment/index'),
        array('value' => 'disparity', 'display_name' => 'Unemployment Gap', 'class' => 'disparity/index'),
        //'element_name' => 'Series',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'subset' => array(
        array('value' => '-', 'display_name' => 'Regular'),
        array('value' => 'std', 'display_name' => 'Standardized'),
        //'element_name' => 'Standardized',
        'placement' => 'field_n',
        'label' => 'Data Series'  
      ),
      'demographic' => $demographic,
      'ethnicity1' => $this->ethnicity_option(),
      'ethnicity2' => $ethnicity,
      'type' => $this->type_option(),
      'start_year' => $start_years,
      'end_year' => $end_years,
      'region' => $region,
      //'year' => $this->years_option(),
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  public function query_all($query_array) {
    $data = hhunempModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];
    if($start_year < $GLOBALS['start_year']) {
      $start_year = $GLOBALS['start_year'];
    } 
    if($end_year > $GLOBALS['end_year']) {
      $end_year = $GLOBALS['end_year'];
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }

    asort($finished);
    return hhunempModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = hhunempModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return hhunempModel::format_JSON_by_state($results);
  }
  
  protected function region_option($request = null, $exception = false, $exception_condition = null) {
    if(!isset($request)) {
      $request = $GLOBALS['request'];
    }
    $data = chart_info('data', $request);

    if(!$data) {
      $data = chart_info('data', $GLOBALS['default_request']);
    } elseif ($exception !== false) {
      if($data == $exception_condition) {
        $data = chart_info($exception);
      }
    }

    $regions = query_db(array('table' => 'household', 'select' => 'region', 'unique' => true, 'conditions' => array('data' => $data), 'order' => array('region')));

    $option = array();
    foreach($regions as $region) {
      $region_name = convert_state($region['region']);
      $option[$region_name] = array('value' => $region['region'], 'display_name' => $region_name);
    }
    ksort($option);
    
    if(isset($option['United States Average'])) {
      $USA = $option['United States Average'];
      unset($option['United States Average']);
      array_unshift($option, $USA);
    }

    if(isset($option['United States'])) {
      $US = $option['United States'];
      unset($option['United States']);
      array_unshift($option, $US);
    }

    $option['placement'] = 'field_h';
    $option['label'] = 'Compare';
    $option['type'] = 'multiselect';

    return $option;
  }
  
}

?>
