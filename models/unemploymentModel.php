<?php

class unemploymentModel extends model {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('data');

    //Format bits of the title
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(isset($title['field_n']) && $title['field_n'] != '') {
      $title['field_n'] = ', ' . $title['field_n'];
    } else {
      $title['field_n'] = '';
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }
    
    
    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . $title['field_n'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . $title['field_n'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . $title['field_n'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . $title['field_n'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . $title['field_n'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . $title['field_n'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    if ($option == 'unemployment' || $option == 'nilf_disc' || $option == 'underemployed') {
      if($type == 'by_state') {
        set('y-axis', 'Percent');
      } else {
        set('y-axis', 'Percentage Point Change');
      }
      set('data_pre', '');
      set('data_post', '%');
    } elseif($option == 'emp_to_pop') {
      if($type == 'by_state') {
        set('y-axis', 'Employment-to-Population Ratio');
      } else {
        set('y-axis', 'Percentage Point Change in Employment-to-Population Ratio');
      }
      set('data_pre', '');
      set('data_post', '%');
    } elseif($option == 'mean_unemp') {
      set('y-axis', 'Weeks');
      set('data_pre', '');
      set('data_post', ' weeks');
    } elseif($option == 'mean_hours') {
      set('y-axis', 'Hours');
      set('data_pre', '');
      set('data_post', ' hours');
    }
  }

  protected function get_details_adv() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('option');
    
    //Format bits of the title
    if(!isset($title['field_b'])) {
      $title['field_b'] = 'In Nominal Dollars';
    }
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(isset($title['regions']) && $title['regions'] != '') {
      $title['field_h'] = $title['regions'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family unemployment [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' Family unemployment ' . $title['field_e'] . ' - ' . $title['field_d'] . ' (' .$title['field_b'] . ')';
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' Family unemployment ' . $title['field_d'] . ' - ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' Family unemployment for ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family unemployment for All States in [F|ELD G] ([FIELD B])
      $view_name = $title['field_a'] . ' Family unemployment for All States in ' . $title['field_g'] . '  (' . $title['field_b'] . ')';
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    if ($option == 'unemployment' || $option == 'nilf_disc' || $option == 'underemployed') {
      set('y-axis', 'Percentage');
      set('data_pre', ' ');
      set('data_post', '%');
    } elseif($option == 'emp_to_pop') {
      set('y-axis', 'Ratio');
      set('data_pre', '');
      set('data_post', ' ');
    } elseif($option == 'mean_unemp') {
      set('y-axis', 'Weeks');
      set('data_pre', '');
      set('data_post', ' ');
    } elseif($option == 'mean_hours') {
      set('y-axis', 'Hours');
      set('data_pre', '');
      set('data_post', ' ');
    }
  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('unemployment', 'index', 'data', 'unemployment', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    } else {
      $default_request = array('unemployment', 'advanced', 'data', 'unemployment', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    }
    set ("default_request", $default_request);
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region[0] = array('value' => 'USA', 'display_name' => 'United States Average');
    
    $start_years = $this->years_option('start');
    $end_years = $this->years_option('end');
    $start_years[] = $end_years[] = array('value' => 2012, 'display_name' => 2012);

    $demographic = array(
      array('value' => 'gender', 'display_name' => 'Gender'),
      array('value' => 'ethnicity', 'display_name' => 'Race/Ethnicity'),
      //'element_name' => 'demographic',
      'placement' => 'field_l',
      'label' => 'Demographic'
    );
    $ethnicity = $this->ethnicity_option();
    $ethnicity['placement'] = 'field_m';
    $views_option = array(
      'data' => array(
        array('value' => 'unemployment', 'display_name' => 'Unemployment Rate', 'class' => 'unemployment/index'),
        array('value' => 'unemp', 'display_name' => 'Household Unemployment', 'class' => 'hhunemp/index'),
        array('value' => 'nilf_disc', 'display_name' => '% Who Are Discouraged', 'class' => 'unemployment/index'),
        array('value' => 'mean_unemp', 'display_name' => 'Mean Unemployment Duration', 'class' => 'unemployment/index'),
        array('value' => 'disparity', 'display_name' => 'Unemployment Gap', 'class' => 'disparity/index'),
        //'element_name' => 'Series',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'subset' => array(
        array('value' => '-', 'display_name' => 'Regular'),
        array('value' => 'std', 'display_name' => 'Standardized'),
        //'element_name' => 'Standardized',
        'placement' => 'field_n',
        'label' => 'Data Series'  
      ),
      'demographic' => $demographic,
      'ethnicity1' => $this->ethnicity_option(),
      'ethnicity2' => $ethnicity,
      'type' => $this->type_option(),
      'start_year' => $start_years,
      'end_year' => $end_years,
      'region' => $region,
      //'year' => $this->years_option(),
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_options_adv() {

  }
  
  public function query_all($query_array) {
    $data = unemploymentModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = chart_info('data');
    if(chart_info('subset') != '') {
      $stat .= '_std';
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year) * 100;
       }
    }
    asort($finished);
    return unemploymentModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = unemploymentModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return unemploymentModel::format_JSON_by_state($results);
  }
  
  protected function query_format($query_array = null, $type_override = false) {
    $request = $GLOBALS['request'];
    
    $start_year = query_db(array('table' => 'jobs', 'select' => 'MIN(year)', 'conditions' => array($query_array['select'][0] . '!=' => 'NULL')));
    $end_year = query_db(array('table' => 'jobs', 'select' => 'MAX(year)', 'conditions' => array($query_array['select'][0] . '!=' => 'NULL')));
    set('start_year', array_pop(array_pop($start_year)));
    set('end_year', array_pop(array_pop($end_year)));
    $request = $GLOBALS['request'];

    if($query_array['conditions']['year'][0] < $GLOBALS['start_year']) {
      $request[array_search($query_array['conditions']['year'][0], $request)] = $query_array['conditions']['year'][0] = $GLOBALS['start_year'];
    } 
    if($query_array['conditions']['year'][1] > $GLOBALS['end_year']) {
      $request[array_search($query_array['conditions']['year'][1], $request)] = $query_array['conditions'] = $GLOBALS['end_year'];
    }
    set('request', $request);
    
    $formatted = array();
    $query_data = query_db($query_array, $type_override);

    foreach($query_data as $array_item) {
      $region = $array_item['region'];
      $year = $array_item['year'];
      unset($array_item['region']);
      unset($array_item['year']);
      if(!array_key_exists($region, $formatted)) {
        foreach($array_item as $field => $value) {
          if (!isset($value) || $value == '') {
            unset($array_item[$field]);
          }
        }
        if (!empty($array_item)) {
          $formatted[$region] = array($year => $array_item);
        }
      } else {
        if (!empty($array_item)) {
          $formatted[$region][$year] = $array_item;
        }
      }
    }
    return $formatted;
  }
  
  protected function years($conditions = array()) {
    $years = array();
    
    $year_query = query_db(array('table' => 'jobs', 'select' => 'year', 'unique' => true, 'conditions' => $conditions));
      foreach($year_query as $year) {
        $years[] = array('value' => $year['year'], 'display_name' =>  $year['year']);
    }
    array_pop($years);
    $this->start_year = array_shift(array_shift($years));
    $this->end_year = array_pop(array_pop($years));
  }

}

?>
