<?php

class rgdpModel extends model2 {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $data = chart_info('data');

    //Format bits of the title
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(!isset($title['field_e']) || $title['field_e'] == '') {
      $title['field_e'] = $GLOBALS['start_year'];
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }
    if($data == 'rgdp') {
      $title['field_a'] = $title['field_b'];
    }
    $title['field_a'] .=  ' RGDP';

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' for ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in RGDP' . $title['field_a'] . ' from ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in RGDP' . $title['field_a'] . ' from ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'RGDP ' . $title['field_a'] . ' for ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    set('y-axis', $title['field_a'] . ' (USD)');
    set('data_pre', '$');
    set('data_post', ' ');
  }
  
  protected function get_defaults() {
    $default_request = array('rgdp', 'index', 'data', 'rgdp', 'stat', 'total', 'type', 'by_state', 'region', 'CA,NY,TX');
    set ("default_request", $default_request);
    return $default_request;
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region['class'] = 'regions';
  
    $views_option = array(
      'data' => array(
        array('value' => 'per_capita', 'display_name' => 'By Worker'),
        array('value' => 'rgdp', 'display_name' => 'By Sector'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'stat' => array(
        array('value' => 'total', 'display_name' => 'Total'),
        array('value' => 'total_private', 'display_name' => 'Private Sector Total'),
        array('value' => 'administrative', 'display_name' => 'Administrative'),
        array('value' => 'agriculture', 'display_name' => 'Agriculture'),
        array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
        array('value' => 'construction', 'display_name' => 'Construction'),
        array('value' => 'durable', 'display_name' => 'Durable Goods'),
        array('value' => 'education', 'display_name' => 'Education'),
        array('value' => 'finance', 'display_name' => 'Finance'),
        array('value' => 'government', 'display_name' => 'Government'),
        array('value' => 'health', 'display_name' => 'Health Care'),
        array('value' => 'hospitality', 'display_name' => 'Hospitality'),
        array('value' => 'information', 'display_name' => 'Information'),
        array('value' => 'management', 'display_name' => 'Management'),
        array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
        array('value' => 'mining', 'display_name' => 'Mining'),
        array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
        array('value' => 'professional', 'display_name' => 'Professional'),
        array('value' => 'real_estate', 'display_name' => 'Real Estate'),
        array('value' => 'retail', 'display_name' => 'Retail'),
        array('value' => 'transportation', 'display_name' => 'Transportation'),
        array('value' => 'utilities', 'display_name' => 'Utilities'),
        array('value' => 'wholesale', 'display_name' => 'Wholesale'),
        array('value' => 'other_services', 'display_name' => 'Other Services'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Sector'
      ),
      'type' => array('value' => 'by_state', 'type'=>'hidden', 'placement' => 'field_f'),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  public function query_all($query_array) {
    $data = rgdpModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];
    if($start_year < $GLOBALS['start_year']) {
      $start_year = $GLOBALS['start_year'];
    } 
    if($end_year > $GLOBALS['end_year']) {
      $end_year = $GLOBALS['end_year'];
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }

    asort($finished);
    return rgdpModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = rgdpModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return rgdpModel::format_JSON_by_state($results);
  }
  
}

?>
