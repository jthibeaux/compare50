<?php

class layoffsModel extends model {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('option');

    //Format bits of the title
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [F|ELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    if(chart_info('type') == 'all') {
      set('y-axis', 'Percentage Point Change in Gap');
    } else {
      set('y-axis', 'Percentage Point Change in Separations');
    }
    set('data_pre', ' ');
    set('data_post', '% ');
  }

  protected function get_details_adv() {  

  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('layoffs', 'index', 'data', 'private', 'type', 'by_state', 'start_year', 1996, 'end_year', 2011, 'region', 'US,CA');
    } else {
      $default_request = array('layoffs', 'advanced', 'data', 'unemployment', 'type', 'by_state', 'start_year', 1996, 'end_year', 2011, 'region', 'CA,US-CA,WR-CA');
    }
    set ("default_request", $default_request);
  }

  protected function get_options() {
    $region = $this->region_option();
    $eliminated_regions = array();
    for ($i = 0; $i < count($region) - 3; $i++) {
      if ($region[$i]['value'] == 'US' || $region[$i]['value'] == 'US-CA' || $region[$i]['value'] == 'WR-CA') {
        $eliminated_regions[] = $i;
      }
    }
    $eliminated_regions = array_reverse($eliminated_regions);
    foreach($eliminated_regions as $eliminated_region) {
      unset($region[$eliminated_region]);
    }
    $region['type'] = 'multiselect';
    $demographic = array(
      array('value' => 'gender', 'display_name' => 'Gender'),
      array('value' => 'ethnicity', 'display_name' => 'Race/Ethnicity'),
      //'element_name' => 'demographic',
      'placement' => 'field_l',
      'label' => 'Demographic'
    );
    $ethnicity = $this->ethnicity_option();
    $ethnicity['placement'] = 'field_m';
  
    $views_option = array(
      'data' => array(
        array('value' => 'private', 'display_name' => 'Separations Relative to Previous Year\'s Employment Private, Non-farm'),
        array('value' => 'business', 'display_name' => 'Separations Relative to Previous Year\'s Employment Private, Non-farm (Business Reasons)'),
        array('value' => 'financial', 'display_name' => 'Separations Relative to Previous Year\'s Employment Private, Non-farm (Financial Reasons)'),
        //'element_name' => 'Separations',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'subset' => array(
        array('value' => '-', 'display_name' => 'Regular'),
        array('value' => 'std', 'display_name' => 'Standardized'),
        //'element_name' => 'Standardized',
        'placement' => 'field_n',
        'label' => 'Data Series'  
      ),
      'type' => $this->type_option(),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_options_adv() {

  }

  protected function region_option() {
    $query = array(
        'select' => 'region',
        'unique' => true
      );
  
    $results = query_db($query);    
    $option = array();
    
    foreach($results as $result) {
      $option[] = array('value' => $result['region'], 'display_name' => convert_state($result['region']));
    }
    $option['element_name'] = 'Regions';
    $option['placement'] = 'field_h';
    $option['label'] = 'Compare';
    
    return $option;
  }
  
  /*protected function years_option($set = null) {
    $option = array();
    for($i = 1996; $i <= 2011; $i++) {*/
      /*if (isset($set) && (($set == 'end' && $i == END_YEAR) || (!$set && $i == START_YEAR))) {
        $option[] = array('value' => $i, 'display_name' => $i, 'selected' => true);
      } else {
        $option[] = array('value' => $i, 'display_name' => $i);
      }*/
      /*$option[] = array('value' => $i, 'display_name' => $i);
    }
    
    if($set == 'start') {
      $option['placement'] = 'field_e';
      $option['label'] = "Range";
    } elseif ($set == 'end') {
      $option['placement'] = 'field_d';
      $option['label'] = "to";
    } else {
      $option['placement'] = 'field_g';
      $option['label'] = "Year";
    }

    return $option;
  }*/
  
  public function query_all($query_array) {
    $data = layoffsModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = 'separations_rel';

    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year) * 100;
       }
    }
    asort($finished);
    return layoffsModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = layoffsModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data) * 100;
      }
    }
  
    return layoffsModel::format_JSON_by_state($results);
  }

}

?>
