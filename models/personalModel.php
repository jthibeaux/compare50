<?php

class personalModel extends model2 {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $data = chart_info('data');

    //Format bits of the title
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(!isset($title['field_e']) || $title['field_e'] == '') {
      $title['field_e'] = $GLOBALS['start_year'];
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' for ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_a'] . ' for ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    set('y-axis', $title['field_a'] . ' (USD)');
    set('data_pre', '$');
    set('data_post', ' ');
  }
  
  protected function get_defaults() {
    $default_request = array('personal', 'index', 'data', 'personal_income', 'type', 'by_state', 'region', 'CA,NY,TX');
    set ("default_request", $default_request);
    return $default_request;
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region['class'] = 'regions';
    
    $start_years = $this->years_option('start');
    $end_years = $this->years_option('end');
    if(chart_info('data') == 'per_capita') {
      $start_years[] = $end_years[] = array('value' => 2012, 'display_name' => 2012);
    }  
  
    $views_option = array(
      'data' => array(
        array('value' => 'personal_income', 'display_name' => 'Personal Income'),
        array('value' => 'per_capita', 'display_name' => 'Per Capita Personal Income'),
        array('value' => 'wage_salary', 'display_name' => 'Wage and Salary Income'),
        array('value' => 'business_income', 'display_name' => "Proprietors Income"),
        //'element_name' => '<TITLE>', 
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'type' => array('value' => 'by_state', 'type'=>'hidden', 'placement' => 'field_f'),
      'start_year' => $start_years,
      'end_year' => $end_years,
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  public function query_all($query_array) {
    $data = personalModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];
    if($start_year < $GLOBALS['start_year']) {
      $start_year = $GLOBALS['start_year'];
    } 
    if($end_year > $GLOBALS['end_year']) {
      $end_year = $GLOBALS['end_year'];
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }

    asort($finished);
    return personalModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = personalModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }

    return personalModel::format_JSON_by_state($results);
  }
  
}

?>
