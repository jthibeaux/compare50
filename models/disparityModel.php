<?php

class disparityModel extends model {

  protected function get_details() {
    
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('option');

    //Format bits of the title
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }
    if($title['field_l'] == 'Gender') {
      $title['field_k'] = 'Men';
      $title['field_m'] = 'Women';
      $title['field_l'] = 'Between Men and Women';
    } 
    if ($title['field_l'] == 'Race/Ethnicity') {
      $title['field_l'] .= ' People';
      //$title['field_k'] .= ' People';
      $title['field_m'] .= ' People';
    }
    $title['field_l'] = 'Between ' . $title['field_k'] . ' and ' . $title['field_m'];

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_l'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_l'] . ' ' . $title['field_l'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_l'] . ' ' . $title['field_l'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [F|ELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_l'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_l'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_l'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    set('y-axis', 'Percentage Point Change in Gap');
    set('data_pre', '');
    set('data_post', '%');
  }
  
  protected function get_options() {
    $region = $this->region_option();
    $eliminated_regions = array();
    for ($i = 0; $i < count($region) - 3; $i++) {
      if (isset($region[$i]['value']) && $region[$i]['value'] == 'PD-CA') {
        $eliminated_regions[] = $i;
      }
    }
    $eliminated_regions = array_reverse($eliminated_regions);
    foreach($eliminated_regions as $eliminated_region) {
      unset($region[$eliminated_region]);
    }

    $region['type'] = 'multiselect';
    $demographic = array(
      array('value' => 'gender', 'display_name' => 'Gender'),
      array('value' => 'ethnicity', 'display_name' => 'Race/Ethnicity'),
      //'element_name' => 'demographic',
      'placement' => 'field_l',
      'label' => 'Demographic'
    );
    $ethnicity = $this->ethnicity_option();
    $ethnicity['placement'] = 'field_m';
    
    $start_years = $this->years_option('start');
    $end_years = $this->years_option('end');
    $start_years[] = $end_years[] = array('value' => 2012, 'display_name' => 2012);
  
    $views_option = array(
      'data' => array(
        array('value' => 'unemployment', 'display_name' => 'Unemployment Rate', 'class' => 'unemployment/index'),
        array('value' => 'unemp', 'display_name' => 'Household Unemployment', 'class' => 'hhunemp/index'),
        array('value' => 'nilf_disc', 'display_name' => '% Who Are Discouraged', 'class' => 'unemployment/index'),
        array('value' => 'mean_unemp', 'display_name' => 'Mean Unemployment Duration', 'class' => 'unemployment/index'),
        array('value' => 'disparity', 'display_name' => 'Unemployment Gap', 'class' => 'disparity/index'),
        //'element_name' => 'Series',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'subset' => array(
        array('value' => '-', 'display_name' => 'Regular'),
        array('value' => 'std', 'display_name' => 'Standardized'),
        //'element_name' => 'Standardized',
        'placement' => 'field_n',
        'label' => 'Data Series'  
      ),
      'demographic' => $demographic,
      'ethnicity1' => $this->ethnicity_option(),
      'ethnicity2' => $ethnicity,
      'type' => $this->type_option(),
      'start_year' => $start_years,
      'end_year' => $end_years,
      'region' => $region,
      //'year' => $this->years_option(),
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('disparity', 'index', 'data', 'disparity', 'demographic', 'gender', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    } else {
      $default_request = array('disparity', 'index', 'data', 'disparity', 'demographic', 'gender', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    }
    set ("default_request", $default_request);
  }
  
  protected function get_options_adv() {

  }
  
  protected function get_details_adv() {
  
  }

  public function query_all($query_array) {
    $data = disparityModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = 'unemployment_rate';
    if(chart_info('demographic') == 'gender') {
      $group1 = 'men';
      $group2 = 'women';
    } else {
      $group1 = chart_info('ethnicity1');
      $group2 = chart_info('ethnicity2');
    }

    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {    
        $first_group = $year_data[$start_year][$group1][$stat] - $year_data[$start_year][$group2][$stat];
        $second_group = $year_data[$end_year][$group1][$stat] - $year_data[$end_year][$group2][$stat];
        $finished[$state] = ($second_group - $first_group)*100;
       }
    }
    
    asort($finished);
    return disparityModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = disparityModel::query_format($query_array);
    if(!$results) {
      return false;
    }
    $finished = array();
    if(chart_info('demographic') == 'gender') {
      $group1 = 'men';
      $group2 = 'women';
    } else {
      $group1 = chart_info('ethnicity1');
      $group2 = chart_info('ethnicity2');
    }
    $field = 'unemployment_rate';

    foreach($results as $region => $year_data) {
      foreach($year_data as $year => $stat) {
        $finished[$region][$year] = $stat[$group1][$field] - $stat[$group2][$field];
      }
    }

    return disparityModel::format_JSON_by_state($finished);
  }

  protected function query_format($query_array = null, $type_override = false) {
    $formatted = array();
    $query_data = query_db($query_array, $type_override);

    foreach($query_data as $array_item) {
      $region = $array_item['region'];
      $year = $array_item['year'];
      $group = $array_item['group'];
      unset($array_item['region']);
      unset($array_item['year']);
      unset($array_item['group']);

      if(!array_key_exists($region, $formatted)) {
        foreach($array_item as $field => $value) {
          if (!isset($value) || $value == '') {
            unset($array_item[$field]);
          }
        }
        if (!empty($array_item)) {
          $formatted[$region][$year] = array($group => $array_item);
        }
      } else {
        if (!empty($array_item)) {
          $formatted[$region][$year][$group] = $array_item;
        }
      }
    }

    return $formatted;
  }

}

?>
