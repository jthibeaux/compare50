<?php

abstract class model2 {

  abstract protected function get_defaults();
  abstract protected function get_details();
  abstract protected function get_options();
  protected $start_year = START_YEAR;
  protected $end_year = END_YEAR;
  
  public function __construct() {
    $request = $GLOBALS['request'];
    $default = $this->get_defaults();
                if(!(($default == null) && isset($GLOBALS['page_override']) && $GLOBALS['page_override'])) {
                  $data = chart_info('data', $request);
                  
                  if(!$data) {
                          $data = chart_info('data', $default);
                  }

                  $start_year = query_db(array('select' => 'MIN(year)', 'conditions' => array('data' => $data)));
                  $end_year = query_db(array('select' => 'MAX(year)', 'conditions' => array('data' => $data)));
                  @$this->start_year = array_pop(array_pop($start_year));
                  @$this->end_year = array_pop(array_pop($end_year));
                  $default[] = 'start_year';
                  $default[] = $this->start_year;
                  $default[] = 'end_year';
                  $default[] = $this->end_year;
                  
                  set('start_year', $this->start_year);
                  set('end_year', $this->end_year);
                  set ("default_request", $default);
                  $this->get_options();
                  $this->get_details();
                }
  }
  
  protected function ethnicity_option() {
    $option = array(
      array('value' => 'asian', 'display_name' => 'Asian'),
      array('value' => 'black', 'display_name' => 'Black'),
      array('value' => 'hispanic', 'display_name' => 'Hispanic'),
      array('value' => 'other', 'display_name' => 'Other'),
      array('value' => 'white', 'display_name' => 'White'),
      array('value' => 'white_nh', 'display_name' => 'White, non-Hispanic'),
      //'element_name' => 'Race/Ethnicity',
      'placement' => 'field_k',
      'label' => 'Race/Ethnicity'
    );
    return $option;
  }

  protected function region_option($request = null, $exception = false, $exception_condition = null) {
    if(!isset($request)) {
      $request = $GLOBALS['request'];
    }
    $data = chart_info('data', $request);

    if(!$data) {
      $data = chart_info('data', $GLOBALS['default_request']);
    } elseif ($exception !== false) {
      if($data == $exception_condition) {
        $data = chart_info($exception);
      }
    }

    $regions = query_db(array('select' => 'region', 'unique' => true, 'conditions' => array('data' => $data), 'order' => array('region')));

    $option = array();
    foreach($regions as $region) {
      $region_name = convert_state($region['region']);
      $option[$region_name] = array('value' => $region['region'], 'display_name' => $region_name);
    }
    ksort($option);
    
    if(isset($option['United States Average'])) {
      $USA = $option['United States Average'];
      unset($option['United States Average']);
      array_unshift($option, $USA);
    }

    if(isset($option['United States'])) {
      $US = $option['United States'];
      unset($option['United States']);
      array_unshift($option, $US);
    }

    $option['placement'] = 'field_h';
    $option['label'] = 'Compare';
    $option['type'] = 'multiselect';

    return $option;
  }
  
  protected function gender_option() {
    $option = array(
      array('value' => 'men', 'display_name' => 'Men'),
      array('value' => 'women', 'display_name' => 'Women'),
      //'element_name' => 'Gender',
      'placement' => 'field_j',
      'label' => 'Gender'
    );
    return $option;
  }
  
  protected function sort_option() {
    $option = array(
      array('value' => '1', 'display_name' => 'By Rank'),
      array('value' => '2', 'display_name' => 'By Region'),
      'placement' => 'field_i',
      'label' => 'Sorting'
    );
    return $option;
  }
  
  protected function submit_option() {
    return array('value' => 'Submit', 'display_name' => 'Submit', 'placement' => 'submit', 'type' => 'submit');
  }
  
  protected function title_elements($request = false) {
    if(!$request) {
      if (count($GLOBALS['request']) > 2) {
        $request = splice_controls(array_slice($GLOBALS['request'], 2));
      } else {
        $request = splice_controls(array_slice($GLOBALS['default_request'], 2));
      }
    }
    $args = array_merge($request['select'], $request['conditions']);
    $title_array = array();
    $unset_array = array();

    for($i = 0; $i < count($args); $i += 2) {
      if (strpos($args[$i + 1], ',')) {
        $conditions = explode(',', $args[$i + 1]);
        $arg_name = $args[$i];
        $unset_array[] = $i;
        $array_arg = '';
        for($j = 0; $j < count($conditions); $j++) {
          if(($j == (count($conditions) - 2))  && (count($conditions) > 2)) {
            $array_arg .= convert_state($conditions[$j]) . ', and ';
          } elseif(($j == (count($conditions) - 2)) && (count($conditions) == 2)) {
            $array_arg .= convert_state($conditions[$j]) . ' and ';
          } elseif(($j != (count($conditions) - 1)) && (count($conditions) > 2)) {
            $array_arg .= convert_state($conditions[$j]) . ', ';
          } else {
            $array_arg .= convert_state($conditions[$j]);
          }
        }
        $title_array[$arg_name] = $array_arg;
      }
    }
    $unset_array = array_reverse($unset_array);
    foreach($unset_array as $unset_item) {
      unset($args[$unset_item + 1]);
      unset($args[$unset_item]);
    }
    $i = 0;
    $args2 = array();
    foreach($args as $arg) {
      $args2[] = $arg;
      $i++;
    }
    $args = $args2;

    error_reporting(E_ERROR | E_PARSE);
    for($i = 0; $i < count($args); $i += 2) {
      foreach($GLOBALS['views_option'][$args[$i]] as $option) {
        if((($option['value'] == $args[$i])) || ($option['value'] == $args[$i + 1])) {
          $title_array[$GLOBALS['views_option'][$args[$i]]['placement']] = $option['display_name'];
        } elseif (!is_array($option) && $option == $args[$i + 1]) {
          $title_array[$GLOBALS['views_option'][$args[$i]]['placement']] = $option;
        } 
      }
    }
    error_reporting(E_ALL);

    return $title_array;
  }
  
  protected function type_option() {
    $option = array(
      array('value' => 'all', 'display_name' => 'Long Term'),
      array('value' => 'by_state', 'display_name' => 'By Year'),
      'placement' => 'field_f',
      'label' => 'Graph Type'
    );
    return $option;
  }
  
  protected function years_option($set = null) {
    $option = array();
    for($i = $this->start_year; $i <= $this->end_year; $i++) {
      $option[] = array('value' => $i, 'display_name' => $i);
    }
        
    if($set == 'start') {
      $option['placement'] = 'field_e';
      $option['label'] = "Range";
    } elseif ($set == 'end') {
      $option['placement'] = 'field_d';
      $option['label'] = "to";
    } else {
      $option['placement'] = 'field_g';
      $option['label'] = "Year";
    }

    return $option;
  }
  
  protected function query_format($query_array = null, $type_override = false) {
    $formatted = array();
    $query_data = query_db($query_array, $type_override);

    foreach($query_data as $array_item) {
      $region = $array_item['region'];
      $year = $array_item['year'];
      unset($array_item['region']);
      unset($array_item['year']);
      if(!array_key_exists($region, $formatted)) {
        foreach($array_item as $field => $value) {
          if (!isset($value) || $value == '') {
            unset($array_item[$field]);
          }
        }
        if (!empty($array_item)) {
          $formatted[$region] = array($year => $array_item);
        }
      } else {
        if (!empty($array_item)) {
          $formatted[$region][$year] = $array_item;
        }
      }
    }
    return $formatted;
  }
  
  protected function format_JSON_all($data) {
    if (!$data) {
      return false;
    }

    $json_data = array();
    $json_categories = array();

    foreach ($data as $state => $year_data) {
      if ($year_data != '') {
        $json_categories[] = convert_state($state);
        if ($state == HIGHLIGHTED) {
          $color = 1;
        } else {
          $color = 0;
        }
        if ($GLOBALS['data_pre'] == '$') {
          $decimal_places = 2;
        } else {
          $decimal_places = 4;
        }
        $json_data[] = "{
          y: " . number_format($year_data, $decimal_places, ".", "") .  ",  
          color: colors[" . $color . "], 
          drilldown: {
            name: '" . $state . "', 
            data: [" . number_format($year_data, $decimal_places, ".", "") . "],  
            color: colors[" . $color . "]
          }
        }"; 
      }
    }

    $json = array(
      'categories' => "['" . implode("', '", $json_categories) . "']",
      'data' => implode(', ', $json_data)
    );

    set('chart_data', $json);
    return true;
  }
    
  public function query_all($query_array) {
    $data = model2::query_format($query_array);
    $finished = array();
    
    foreach($data as $state => $years) {
      if(count($years) === 2 && is_state($state) !== false) {
        $first_year = array_shift(array_shift($years));
        $second_year = array_shift(array_shift($years));
        $finished[$state] = ($second_year - $first_year) / $first_year * 100;
       }
    }
    asort($finished);
    return $finished;
  }
  
  protected function format_JSON_by_state($data) {
    if (!$data) {
      return false;
    }
  
    $json = array();
    $json_data = array();

    foreach ($data as $state => $year_data) {
      if ($GLOBALS['data_pre'] == '$') {
        $decimal_places = 2;
      } else {
        $decimal_places = 4;
      }
      if ($GLOBALS['data_post'] == '%') {
        $multiplier = 100;
      } else {
        $multiplier = 1;
      }
      if (!effectively_empty($year_data)) {
        if (!isset($json['categories']) || !$json['categories']) {
          $json['categories'] = "['" . implode("', '", array_keys($year_data)) . "']";
        }
        foreach($year_data as $year => $value) {
          $year_data[$year] = number_format($value * $multiplier, $decimal_places, '.', '');
        }
  
        $json_data[] = "{
        name: '" . convert_state($state) . "',
        data: [" . implode(", ", $year_data) . "]
        }";
      }
    }
    
    $json['data'] = implode(", ", $json_data);
  
    set('chart_data', $json);
    return true;
  }
    
  function query_by_state($query_array, $special_lines = false) {
    $results = model2::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }

    return $results;
  }

}

?>
