<?php

class demographicsModel extends model2 {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $data = chart_info('data');

    //Format bits of the title
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(!isset($title['field_e']) || $title['field_e'] == '') {
      $title['field_e'] = $GLOBALS['start_year'];
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ', ' . $title['field_a'] . ' ' . $title['field_b'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ', ' . $title['field_a'] . ' ' . $title['field_b'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ', ' . $title['field_a'] . ' ' . $title['field_b'] . ' ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a']  . ' Demographic ' . $title['field_b'] . ' from ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a']  . ' Demographic ' . $title['field_b'] . ' from ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_a'] . ' for ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    if ($data == 'income' && in_array('median', $GLOBALS['request'])) {
      set('y-axis', 'Earnings (USD)');
      set('data_pre', '$');
      set('data_post', ' ');
    } elseif(chart_info('values') == 'percent') {
      set('y-axis', 'Percentage');
      set('data_pre', ' ');
      set('data_post', '%');
    } else {
      set('y-axis', 'Persons');
      set('data_pre', ' ');
      set('data_post', ' ');
    }
  }
  
  protected function get_defaults() {
    $default_request = array('demographics', 'index', 'data', 'ethnicity', 'stat', 'asian', 'values', 'percent', 'type', 'by_state', 'region', 'CA,NY,TX');
    set ("default_request", $default_request);
    return $default_request;
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region['class'] = 'regions';
  
    $views_option = array(
      'data' => array(
        array('value' => 'ethnicity', 'display_name' => 'Race/Ethnicity'),
        array('value' => 'age', 'display_name' => 'Age'),
        array('value' => 'income', 'display_name' => 'Household Income'),
        array('value' => 'education', 'display_name' => 'Education Levels'),
        array('value' => 'tenure', 'display_name' => 'Tenure'),
        array('value' => 'origin', 'display_name' => 'Country of Origin'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'ethnicity' => array(
        array('value' => 'asian', 'display_name' => 'Asian'),
        array('value' => 'black', 'display_name' => 'Black'),
        array('value' => 'hispanic', 'display_name' => 'Hispanic'),
        array('value' => 'multiple', 'display_name' => 'Multiple'),
        array('value' => 'native_am', 'display_name' => 'Native American'),
        array('value' => 'other', 'display_name' => 'Other'),
        array('value' => 'pa_islander', 'display_name' => 'Pacific Islander'),
        array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'age' => array(
        array('value' => '17-under', 'display_name' => '17 and Under'),
        array('value' => '18-24', 'display_name' => '18 - 24'),
        array('value' => '25-44', 'display_name' => '25 - 44'),
        array('value' => '45-64', 'display_name' => '45 - 64'),
        array('value' => '65-over', 'display_name' => '65 and Over'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Age Group'
      ),
      'income' => array(
        array('value' => 'median', 'display_name' => 'Median'),
        array('value' => '25k-', 'display_name' => '$24,999 and Under'),
        array('value' => '25k-50k', 'display_name' => '$25,000 - $49,999'),
        array('value' => '50k-75k', 'display_name' => '$50,000 - $74,999'),
        array('value' => '75k-100k', 'display_name' => '$75,000 - $99,999'),
        array('value' => '100k+', 'display_name' => '$100,000 and Over'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Income Bracket'
      ),
      'tenure' => array(
        array('value' => 'renter', 'display_name' => 'Renter-Occupied'),
        array('value' => 'owner', 'display_name' => 'Owner-Occupied'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Tenure'
      ),
      'education' => array(
        array('value' => 'no_hs', 'display_name' => 'No High School'),
        array('value' => 'no_diploma', 'display_name' => 'Some High School'),
        array('value' => 'hs_diploma', 'display_name' => 'High School Diploma'),
        array('value' => 'some_college', 'display_name' => 'Some College'),
        array('value' => 'associates', 'display_name' => 'Associates Degree'),
        array('value' => 'bachelors', 'display_name' => 'Bachelors Degree'),
        array('value' => 'postgrad', 'display_name' => 'Postgraduate or Professional Degree'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Education Levels'
      ),
      'origin' => array(
        array('value' => 'africa', 'display_name' => 'Africa'),
        array('value' => 'asia', 'display_name' => 'Asia'),
        array('value' => 'europe', 'display_name' => 'Europe'),
        array('value' => 'latin_america', 'display_name' => 'Latin America'),
        array('value' => 'north_america', 'display_name' => 'North America'),
        array('value' => 'oceania', 'display_name' => 'Oceania'),
        array('value' => 'total', 'display_name' => 'Total'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Continents'
      ),
      'values' => array(
        array('value' => 'percent', 'display_name' => '% of Population'),
        array('value' => 'number', 'display_name' => 'Number of People'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_o',
        'label' => 'Sort'
      ),
      'type' => array('value' => 'by_state', 'type'=>'hidden', 'placement' => 'field_f'),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  public function query_all($query_array) {
    $data = demographicsModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];
    if($start_year < $GLOBALS['start_year']) {
      $start_year = $GLOBALS['start_year'];
    } 
    if($end_year > $GLOBALS['end_year']) {
      $end_year = $GLOBALS['end_year'];
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }

    asort($finished);
    return demographicsModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = demographicsModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return demographicsModel::format_JSON_by_state($results);
  }
  
}

?>
