<?php

class cesModel extends model {

  protected function get_details() {
    
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('data');

    //Format bits of the title
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' in ' . $title['field_e'];
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [F|ELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'];
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'];
      } else {
        $view_name = 'Change in ' . $title['field_a'] . ' in ' . $title['field_e'];
      }
    }

    set('view_name', $view_name);
    
    //Format the data type and Y-axis title
    set('y-axis', 'Growth');
    set('data_pre', '');
    set('data_post', '%');
  }
  
  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    
    $years_start =  $this->years_option('start');
    $years_end =  $this->years_option('end');
    unset($years_start[0]);
    unset($years_end[0]);  
  
    $views_option = array(
      'data' => array(
        array('value' => 'real_gsp_percapita_growth', 'display_name' => 'Per Capita Real GSP Growth (all industries)', 'class' => 'growth/index'),
        array('value' => 'real_gsp_growth', 'display_name' => 'Real GSP Growth (all industries)', 'class' => 'growth/index'),
        array('value' => 'gsp_labors_share', 'display_name' => "Labor's Share of GSP (all industries)", 'class' => 'growth/index'),
        array('value' => 'all', 'display_name' => 'Employment Growth (all industries, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'all_private', 'display_name' => 'Employment Growth (all industries, private, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'manufacturing_all_private', 'display_name' => 'Employment Growth (manufacturing, private, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'tech', 'display_name' => 'Employment Growth (professional, scientific, and technical services, QCEW data)', 'class' => 'qcew/index'),
        array('value' => 'total_nonfarm', 'display_name' => 'Non-farm Employment Growth (CES data)', 'class' => 'ces/index'),
        array('value' => 'total_private', 'display_name' => 'Private Employment Growth (CES data)', 'class' => 'ces/index'),
        array('value' => 'wage_all', 'display_name' => 'Wage Growth (all industries)', 'class' => 'qcew/index'),
        array('value' => 'wage_all_private', 'display_name' => 'Wage Growth (all industries, private)', 'class' => 'qcew/index'),
        array('value' => 'wage_manufacturing_all_private', 'display_name' => 'Wage Growth (manufacturing, private)', 'class' => 'qcew/index'),
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'type' => $this->type_option(),
      'start_year' => $years_start,
      'end_year' => $years_end,
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('ces', 'index', 'data', 'total_private', 'type', 'by_state', 'start_year', START_YEAR+1, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    } else {
      $default_request = array('ces', 'advanced', 'data', 'total_private', 'type', 'by_state', 'start_year', START_YEAR+1, 'end_year', END_YEAR, 'region', 'CA,NY,TX');
    }
    set ("default_request", $default_request);
  }
  
  protected function get_options_adv() {

  }
  
  protected function get_details_adv() {
  
  }
  
  /*protected function region_option() {
    $query = array(
        'select' => 'region',
        'unique' => true,
        'order' => 'region'
      );
  
    $results = query_db($query);    
    $option = array();
    debug($results);
    foreach($results as $result) {
      $option[] = array('value' => $result['region'], 'display_name' => convert_state($result['region']));
    }
    $option['element_name'] = 'Regions';
    $option['placement'] = 'field_h';
    $option['label'] = 'Compare';
    
    return $option;
  }*/
  
  public function query_all($query_array) {
    $data = cesModel::query_format($query_array);
    $finished = array();

    foreach($data as $state => $years) {
      if(count($years) === 2 && is_state($state) !== false) {
        $first_year = array_shift(array_shift($years));
        $second_year = array_shift(array_shift($years));
        $finished[$state] = ($second_year - $first_year) / $first_year * 100;
       }
    }
    asort($finished);
    return cesModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array) {
    $results = cesModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }

    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return cesModel::format_JSON_by_state($results);
  }

}

?>
