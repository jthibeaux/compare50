<?php

class rankModel extends model2 {

  public function __construct() {
    $request = $GLOBALS['request'];
                unset($request[0]);
                unset($request[1]);
    $default = $this->get_defaults();

                if(!(($default == null) && isset($GLOBALS['page_override']) && $GLOBALS['page_override'])) {
                  $start_year = query_db(array('select' => 'MIN(year)', 'conditions' => array('data' => $request)));
                  $end_year = query_db(array('select' => 'MAX(year)', 'conditions' => array('data' => $request)));
                  @$this->start_year = array_pop(array_pop($start_year));
                  @$this->end_year = array_pop(array_pop($end_year));
                  
                  set('start_year', $this->start_year);
                  set('end_year', $this->end_year);
                  set("default_request", $default);
                  $this->get_options();
                  $this->get_details();
                }
  }
  
  protected function get_details() {  
    set('view_name', 'State Ranker');

    //Format the data type and Y-axis title
                set('y-axis', 'Weighted Ranks');
                set('data_pre', ' ');
                set('data_post', ' ');
  }
  
  protected function get_defaults() {
          if(count($GLOBALS['request']) < 3) {
            set('page_override', true); 
          } else {
            set('show_options', false);
          }

          return null;
  }

        public function do_get_details() {
                @rankModel::get_details();
        }

        public function do_get_options() {
    $views_option = array(
      'data' => array(
                                array('value' => 'income', 'display_name' => 'Personal Income'),
                                array('value' => 'labor_share', 'display_name' => 'Labor Share of Real GDP'),
                                array('value' => 'percapita_share', 'display_name' => 'Per capita real GDP'),
                                array('value' => 'rgdp', 'display_name' => 'Real GDP'),
                                array('value' => 'rgdp_qindex', 'display_name' => 'Real GDP Quantity Index'),
                                array('value' => 'federal', 'display_name' => 'Federal'),
                                array('value' => 'local', 'display_name' => 'Local'),
                                array('value' => 'state', 'display_name' => 'State'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force Statistics'),
                                array('value' => 'avg_wage', 'display_name' => 'Average Annual Wage'),
                                array('value' => 'delta_avg_wage', 'display_name' => 'Average Annual Wage Change'),
                                array('value' => 'emp_growth', 'display_name' => 'Employment Growth since 2002'),
                                array('value' => 'est_growth', 'display_name' => 'Establishment Growth since 2002'),
                                array('value' => 'real_avg_wage', 'display_name' => 'Real Average Annual Wage ($2002)'),
                                array('value' => 'delta_real_avg_change', 'display_name' => 'Real Average Annual Wage Change ($2002)'),
                                array('value' => 'population', 'display_name' => 'Population'),
                                array('value' => 'unemp_disc', 'display_name' => 'Unemployed or Discouraged Rate'),
                                array('value' => 'unemp_disc_underemp', 'display_name' => 'Unemployed Discouraged or Underemployed Rate'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'f', 'display_name' => 'Female'),
                                array('value' => 'f_asian', 'display_name' => 'Female and Asian'),
                                array('value' => 'f_black', 'display_name' => 'Female and Black'),
                                array('value' => 'f_hisp', 'display_name' => 'Female and Hispanic'),
                                array('value' => 'f_other', 'display_name' => 'Female and Other'),
                                array('value' => 'f_white', 'display_name' => 'Female and White'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'm', 'display_name' => 'Male'),
                                array('value' => 'm_asian', 'display_name' => 'Male and Asian'),
                                array('value' => 'm_black', 'display_name' => 'Male and Black'),
                                array('value' => 'm_hisp', 'display_name' => 'Male and Hispanic'),
                                array('value' => 'm_other', 'display_name' => 'Male and Other'),
                                array('value' => 'm_white', 'display_name' => 'Male and White'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
                                array('value' => 'median_income', 'display_name' => 'Median Household Income'),
                                array('value' => 'median_income_adj', 'display_name' => 'Median Household Income, Inflation Adjusted'),
                                array('value' => 'alt_pov', 'display_name' => 'Alternative Poverty Rate'),
                                array('value' => 'pov', 'display_name' => 'Poverty Rate'),
                                array('value' => 'per_100k', 'display_name' => 'Rate per 100,000 inhabitants'),
                                array('value' => 'total_crime', 'display_name' => 'Total Crimes'),
                                array('value' => 'grad', 'display_name' => 'High School Graduation Rate'),
                                array('value' => 'debt', 'display_name' => 'Debt'),
                                array('value' => 'delinquency', 'display_name' => 'Delinquency'),
                                array('value' => 'forclosures', 'display_name' => 'Foreclosures Started'),
                                array('value' => 'patents', 'display_name' => 'Patents'),
                                array('value' => 'exports', 'display_name' => 'Goods Exports'),
                                array('value' => 'fam_income', 'display_name' => 'Family Income'),
                                array('value' => 'fam_income_adj', 'display_name' => 'Family Income Inflation Adjusted'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'fam_income' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'fam_income_adj' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'patents' => array(
                                array('value' => 'pop_adj', 'display_name' => 'Population Adjusted'),
                                array('value' => 'patents', 'display_name' => 'Total Patents'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Patents'
      ),
      'debt' => array(
                                array('value' => 'household_debt_ratio', 'display_name' => 'Household Debt as a Share of Average Wage'),
                                array('value' => 'household_debt', 'display_name' => 'Total Household Debt'),
                                array('value' => 'auto', 'display_name' => 'Automobile'),
                                array('value' => 'credit', 'display_name' => 'Credit Card'),
                                array('value' => 'mortgage', 'display_name' => 'Mortgage'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Debt'
      ),
      'total_crime' => array(
                                array('value' => 'property', 'display_name' => 'Property'),
                                array('value' => 'violent', 'display_name' => 'Violent'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Total Crime'
      ),
      'per_100k' => array(
                                array('value' => 'property', 'display_name' => 'Property'),
                                array('value' => 'violent', 'display_name' => 'Violent'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Total Crime Per 100k'
      ),
      'pov' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'alt_pov' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'median_income_adj' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'median_income' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity'
      ),
      'white' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'total' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'other' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm_white' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm_other' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm_hisp' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm_black' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm_asian' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'm' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f_white' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f_other' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f_hisp' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f_black' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f_asian' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'f' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'black' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'asian' => array(
                                array('value' => 'hourly_avg', 'display_name' => 'Hourly Average Earnings'),
                                array('value' => 'hourly_avg_adj', 'display_name' => 'Hourly Average Earnings Inflation Adjusted'),
                                array('value' => 'weekly_avg', 'display_name' => 'Weekly Average Earnings'),
                                array('value' => 'weekly_avg_adj', 'display_name' => 'Weekly Average Earnings Inflation Adjusted'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Earnings'
      ),
      'unemp' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'f', 'display_name' => 'Female'),
                                array('value' => 'f_asian', 'display_name' => 'Female and Asian'),
                                array('value' => 'f_black', 'display_name' => 'Female and Black'),
                                array('value' => 'f_hisp', 'display_name' => 'Female and Hispanic'),
                                array('value' => 'f_other', 'display_name' => 'Female and Other'),
                                array('value' => 'f_white', 'display_name' => 'Female and White'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'm', 'display_name' => 'Male'),
                                array('value' => 'm_asian', 'display_name' => 'Male and Asian'),
                                array('value' => 'm_black', 'display_name' => 'Male and Black'),
                                array('value' => 'm_hisp', 'display_name' => 'Male and Hispanic'),
                                array('value' => 'm_other', 'display_name' => 'Male and Other'),
                                array('value' => 'm_white', 'display_name' => 'Male and White'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity/Sex'
      ),
      'unemp_disc_underemp' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'f', 'display_name' => 'Female'),
                                array('value' => 'f_asian', 'display_name' => 'Female and Asian'),
                                array('value' => 'f_black', 'display_name' => 'Female and Black'),
                                array('value' => 'f_hisp', 'display_name' => 'Female and Hispanic'),
                                array('value' => 'f_other', 'display_name' => 'Female and Other'),
                                array('value' => 'f_white', 'display_name' => 'Female and White'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'm', 'display_name' => 'Male'),
                                array('value' => 'm_asian', 'display_name' => 'Male and Asian'),
                                array('value' => 'm_black', 'display_name' => 'Male and Black'),
                                array('value' => 'm_hisp', 'display_name' => 'Male and Hispanic'),
                                array('value' => 'm_other', 'display_name' => 'Male and Other'),
                                array('value' => 'm_white', 'display_name' => 'Male and White'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity/Sex'
      ),
      'unemp_disc' => array(
                                array('value' => 'asian', 'display_name' => 'Asian'),
                                array('value' => 'black', 'display_name' => 'Black'),
                                array('value' => 'f', 'display_name' => 'Female'),
                                array('value' => 'f_asian', 'display_name' => 'Female and Asian'),
                                array('value' => 'f_black', 'display_name' => 'Female and Black'),
                                array('value' => 'f_hisp', 'display_name' => 'Female and Hispanic'),
                                array('value' => 'f_other', 'display_name' => 'Female and Other'),
                                array('value' => 'f_white', 'display_name' => 'Female and White'),
                                array('value' => 'hisp', 'display_name' => 'Hispanic'),
                                array('value' => 'm', 'display_name' => 'Male'),
                                array('value' => 'm_asian', 'display_name' => 'Male and Asian'),
                                array('value' => 'm_black', 'display_name' => 'Male and Black'),
                                array('value' => 'm_hisp', 'display_name' => 'Male and Hispanic'),
                                array('value' => 'm_other', 'display_name' => 'Male and Other'),
                                array('value' => 'm_white', 'display_name' => 'Male and White'),
                                array('value' => 'other', 'display_name' => 'Other'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'white', 'display_name' => 'White'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Race/Ethnicity/Sex'
      ),
      'population' => array(
                                array('value' => 'pop_density', 'display_name' => 'Population Density'),
                                array('value' => 'pop_total', 'display_name' => 'Total Population'),
        //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Population'
      ),
                        'delta_real_avg_change' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'real_avg_wage' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'est_growth' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'emp_growth' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'delta_avg_wage' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'avg_wage' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'labor_force' => array(
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'unemployment', 'display_name' => 'Unemployed'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'local' => array(
                                array('value' => 'percapita_taxes', 'display_name' => 'Per-Capita Taxes'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Taxes'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Share'
                        ),
                        'state' => array(
                                array('value' => 'percapita_taxes', 'display_name' => 'Per-Capita Taxes'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Taxes'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Share'
                        ),
                        'federal' => array(
                                array('value' => 'percapita_taxes', 'display_name' => 'Per-Capita Taxes'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Taxes'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Share'
                        ),
                        'rgdp' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'avg_wage' => array(
                                array('value' => 'hospitality', 'display_name' => 'Accommodation'),
                                array('value' => 'administrative', 'display_name' => 'Administrative'),
                                array('value' => 'agriculture', 'display_name' => 'Agriculture'),
                                array('value' => 'arts', 'display_name' => 'Arts and Ent'),
                                array('value' => 'construction', 'display_name' => 'Construction'),
                                array('value' => 'durable', 'display_name' => 'Durable Goods'),
                                array('value' => 'education', 'display_name' => 'Education'),
                                array('value' => 'finance', 'display_name' => 'Finance'),
                                array('value' => 'government', 'display_name' => 'Government'),
                                array('value' => 'health', 'display_name' => 'Health Care'),
                                array('value' => 'information', 'display_name' => 'Information'),
                                array('value' => 'management', 'display_name' => 'Management'),
                                array('value' => 'manufacturing', 'display_name' => 'Manufacturing'),
                                array('value' => 'mining', 'display_name' => 'Mining'),
                                array('value' => 'nondurable', 'display_name' => 'Nondurable Goods'),
                                array('value' => 'other_services', 'display_name' => 'Other Services'),
                                array('value' => 'professional', 'display_name' => 'Professional'),
                                array('value' => 'real_estate', 'display_name' => 'Real Estate'),
                                array('value' => 'retail', 'display_name' => 'Retail Trade'),
                                array('value' => 'total_private', 'display_name' => 'Total Private'),
                                array('value' => 'transportation', 'display_name' => 'Transportation'),
                                array('value' => 'utilities', 'display_name' => 'Utilities'),
                                array('value' => 'wholesale', 'display_name' => 'Wholesale Trade'),
                                array('value' => 'percapita_taxes', 'display_name' => 'Current Taxes per Capita'),
                                array('value' => 'total_taxes', 'display_name' => 'Total Current Taxes'),
                                array('value' => 'employment', 'display_name' => 'Employment'),
                                array('value' => 'labor_force', 'display_name' => 'Labor Force'),
                                array('value' => 'unemp', 'display_name' => 'Unemployment'),
                                array('value' => 'unemp_rate', 'display_name' => 'Unemployment Rate'),
                                array('value' => 'arts', 'display_name' => 'Arts and Entertainment'),
                                array('value' => 'education', 'display_name' => 'Education Services'),
                                array('value' => 'government_fed', 'display_name' => 'Federal Government'),
                                array('value' => 'finance', 'display_name' => 'Finance and Insurance'),
                                array('value' => 'government_loc', 'display_name' => 'Local Government'),
                                array('value' => 'management', 'display_name' => 'Management of Enterprises'),
                                array('value' => 'private', 'display_name' => 'Private Industries'),
                                array('value' => 'government_st', 'display_name' => 'State Government'),
                                array('value' => 'total', 'display_name' => 'Total'),
                                array('value' => 'unclassified', 'display_name' => 'Unclassified'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Industry'
                        ),
                        'income' => array(
                                array('value' => 'percapita_income', 'display_name' => 'Per Capita Personal Income'),
                                array('value' => 'income', 'display_name' => 'Personal Income'),
                                array('value' => 'proprietary_income', 'display_name' => 'Proprietors\' income'),
                                array('value' => 'wages', 'display_name' => 'Wage and salary disbursements'),
                                //'element_name' => '<TITLE>',
        'placement' => 'field_b',
        'label' => 'Share'
                        ),
    );
    set('views_option', $views_option);
        }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
    $region['class'] = 'regions';
                $this->do_get_options();  
  }
  
  public function query_all($query_array) {
    $data = rankModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];
    if($start_year < $GLOBALS['start_year']) {
      $start_year = $GLOBALS['start_year'];
    } 
    if($end_year > $GLOBALS['end_year']) {
      $end_year = $GLOBALS['end_year'];
    }
    
    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }

    asort($finished);
                
    return rankModel::format_JSON_all($finished);
  }
  
  protected function query_format($query_array = null, $type_override = false) {
    $formatted = array();
    $query_data = query_db($query_array, $type_override);

                $data = rankModel::arrange_data($query_data);
                $data = rankModel::compute_score($data);
                $options = rankModel::get_states($data);
                $data = rankModel::format_JSON_all($data);
        }

        private function get_states($data) {
          $states = array_keys($data);
          $state_options = array();
          foreach($states as $state) {
            $state_options[$state] = convert_state($state);
          }
          asort($state_options);
          set('option_states', $state_options);
        }

        private function arrange_data($query_data) {
                $data = array();

                foreach($query_data as $data_item) {
                  if(!isset($data[$data_item['region']])) {
                    $data[$data_item['region']] = array();   
                  }
                  if(!isset($data[$data_item['region']][$data_item['data']])) {
                    $data[$data_item['region']][$data_item['data']] = array();
                  }
                  $data[$data_item['region']][$data_item['data']][$data_item['stat']] = $data_item['value'];
                }
                return $data;
        }

        private function compute_score($data) {
                $ranked = true;
                $args = $GLOBALS['request'];
                if($args[count($args) - 1] == 'unranked') {
                  $ranked = false;
                }

                if(!isset($GLOBALS['priorities'])) {
                  return false;
                }
                $priorities = $GLOBALS['priorities'];

                $ranks = array();
                $full_data = array();
                foreach($priorities as $order => $priority) {
                  foreach($data as $state => $info) {
                    if(!isset($ranks[$state])) {
                      $ranks[$state] = 0;
                      $full_data[$state] = array();
                    }
                    if($ranked) {
                      $ranks[$state] += ($info[$priority['data']][$priority['stat']] * (10-$order));
                    } else {
                      $ranks[$state] += $info[$priority['data']][$priority['stat']];
                    }
                    $full_data[$state][$order] = $info[$priority['data']][$priority['stat']];
                  }
                }

                set('full_data', json_encode($full_data));
                asort($ranks);
                foreach($ranks as $state => $rank) {
                  $ranks[$state] = (max($ranks) - $rank);
                }

                return $ranks; 
        }

}

?>
