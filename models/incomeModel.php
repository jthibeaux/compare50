<?php

class incomeModel extends model {

  protected function get_details() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('option');

    //Format bits of the title
    if(!is_advanced()) {
        $title['field_b'] = 'In 2011 Dollars';
    } elseif (!isset($title['field_b'])) {
      $title['field_b'] = 'In Nominal Dollars';
    }
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(isset($title['region']) && $title['region'] != '') {
      $title['field_h'] = $title['region'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'] . ' (' .$title['field_b'] . ')';
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' for ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] for All States in [FIELD G] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_e'] . ' - ' . $title['field_d'] . ' (' .$title['field_b'] . ')';
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = 'Change in ' . $title['field_a'] . ' from ' . $title['field_d'] . ' - ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      } else {
        $view_name = $title['field_a'] . ' for ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      }
    }

    set('view_name', $view_name);

    //Format the data type and Y-axis title
    /*if ($option == 'growth' || $type == 'all') {
      set('y-axis', 'Percentage of growth');
      set('data_pre', ' ');
      set('data_post', '%');
    } else {*/
      set('y-axis', 'Income (USD)');
      set('data_pre', '$');
      set('data_post', ' ');
    //}
  }

  protected function get_details_adv() {  
    $title = $this->title_elements();
    $type = chart_info('type');
    $option = chart_info('option');

    //Format bits of the title
    if(!isset($title['field_b'])) {
      $title['field_b'] = 'In Nominal Dollars';
    }
    if(isset($title['field_c']) && $title['field_c'] != '') {
      $title['field_c'] = '(in ' . $title['field_c'] . ')';
    }
    if(isset($title['regions']) && $title['regions'] != '') {
      $title['field_h'] = $title['regions'];
    }

    if($type == 'by_state') {
      //<FIELD F = by_state> [FIELD H as REGIONS] [FIELD A] [FIELD B] Family Income [FIELD E] - [FIELD D] ([FIELD B])
      if ($title['field_e'] < $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_b'] . ' ' . $title['field_e'] . ' - ' . $title['field_d'] . ' (' .$title['field_b'] . ')';
      } elseif ($title['field_e'] > $title['field_d']) {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_b'] . ' ' . $title['field_d'] . ' - ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      } else {
        $view_name = $title['field_h'] . ' ' . $title['field_a'] . ' ' . $title['field_a'] . ' for ' . $title['field_e'] . ' (' . $title['field_b'] . ')';
      }

    } elseif ($type == 'all') {
      //<FIELD F = all> [FIELD A] [FIELD B] of Family Income for All States in [F|ELD G] ([FIELD B])
      $view_name = $title['field_a'] . ' for All States in ' . $title['field_g'] . '  (' . $title['field_b'] . ')';
    }

    set('view_name', $view_name);
  }
  
  protected function get_defaults() {
    if(!is_advanced()) {
      $default_request = array('income', 'index', 'data', 'percentile_50_adj', 'option', 'adj', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'US,CA');
    } else {
      $default_request = array('income', 'advanced', 'percentile', '50', 'option', 'adj', 'type', 'by_state', 'start_year', START_YEAR, 'end_year', END_YEAR, 'region', 'US,CA');
    }
    set ("default_request", $default_request);
  }

  protected function get_options() {
    $region = $this->region_option();
    $region['type'] = 'multiselect';
  
    $views_option = array(
      'data' => array(
        array('value' => 'percentile_10_adj', 'display_name' => '10th Percentile Family Income'),
        array('value' => 'percentile_25_adj', 'display_name' => '25th Percentile Family Income'),
        array('value' => 'percentile_50_adj', 'display_name' => 'Median Family Income'),
        array('value' => 'percentile_75_adj', 'display_name' => '75th Percentile Family Income'),
        array('value' => 'percentile_90_adj', 'display_name' => '90th Percentile Family Income'),
        array('value' => 'differential_50-10', 'display_name' => '50-10 Family Income Differential'),
        array('value' => 'differential_90-50', 'display_name' => '90-50 Family Income Differential'),
        array('value' => 'differential_90-10', 'display_name' => '90-10 Family Income Differential'),
        //'element_name' => 'Family Income',
        'placement' => 'field_a',
        'label' => 'Data Series'
      ),
      'type' => $this->type_option(),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      'submit' => $this->submit_option()
    );
    
    set('views_option', $views_option);
  }
  
  protected function get_options_adv() {
    /*$region = $this->region_option();
    $region['type'] = 'multiselect';
  
    $views_option = array(
      'percentile' => array(
        array('value' => 10, 'display_name' => '10th Percentile'),
        array('value' => 25, 'display_name' => '25th Percentile'),
        array('value' => 50, 'display_name' => 'Median'),
        array('value' => 75, 'display_name' => '75th Percentile'),
        array('value' => 90, 'display_name' => '90th Percentile'),
        'element_name' => 'Percentile',
        'placement' => 'field_a',
        'label' => 'Income Percentiles'
      ),
      'differential' => array(
        array('value' => '50-10', 'display_name' => '50-10 Differential'),
        array('value' => '90-50', 'display_name' => '90-50 Differential'),
        array('value' => '90-10', 'display_name' => '90-10 Differential'),
        'element_name' => 'Differential',
        'placement' => 'field_a',
        'label' => 'Income Differentials'
      ),
      'option' => array(
        array('value' => '-', 'display_name' => 'Nominal Dollars'),
        array('value' => 'adj', 'display_name' => 'In 2011 Dollars'),
        array('value' => 'growth', 'display_name' => 'Percentage of Growth'),
        'placement' => 'field_b',
        'label' => 'Option'
      ),
      'type' => $this->type_option(),
      'start_year' => $this->years_option('start'),
      'end_year' => $this->years_option('end'),
      'region' => $region,
      'year' => $this->years_option(),
      'submit' => $this->submit_option(),
      'sort' => $this->sort_option()
    );
    
    set('views_option', $views_option);*/
  }

  public function query_all($query_array) {
    $data = incomeModel::query_format($query_array);
    $finished = array();
    $start_year = chart_info('start_year');
    $end_year = chart_info('end_year');
    $stat = $query_array['select'][0];

    foreach($data as $state => $year_data) {
      if(count($year_data) === 2 && is_state($state) !== false) {
        $first_year = $year_data[$start_year][$stat];
        $second_year = $year_data[$end_year][$stat];
        $finished[$state] = ($second_year - $first_year);
       }
    }
    asort($finished);
    return incomeModel::format_JSON_all($finished);
  }
  
  public function query_by_state($query_array, $special_lines = false) {
    $results = @incomeModel::query_format($query_array);
    
    if(!$results) {
      return false;
    }
  
    foreach($results as $region => $region_data) {
      foreach($region_data as $year => $data) {
        $results[$region][$year] = array_shift($data);
      }
    }
  
    return incomeModel::format_JSON_by_state($results);
  }
  
}

?>
