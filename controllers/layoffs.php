<?php

class layoffs extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //Select
      $select = array();
      $select[] = 'separations_rel';
      if(in_array('data', $args['select'])) {
        $item_location = array_search('data', $args['select']);
        array_unshift($args['conditions'], $args['select'][$item_location + 1]);
        array_unshift($args['conditions'], $args['select'][$item_location]);
        unset($args['select'][$item_location]);
        unset($args['select'][$item_location + 1]);
      }
      
      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_layoffs` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_layoffs` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = layoffs::process_conditions($args['conditions']);
      $select[] = 'region';
      $select[] = 'year';

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
/*  public function advanced($args = null) {

  }*/

}

?>
