<?php

class growth extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //Select
      $select = array();
      $select[] = $args['select'][1];
      $select[] = 'region';
      $select[] = 'year';

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_growth` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_growth` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $query = array(
        'select' => $select,
        'conditions' => growth::process_conditions($args['conditions']),
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }

    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  
  /*public function advanced($args = null) {
  
  }*/
    
}

?>
