<?php

class ces extends controller {

  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  

      $select = array();
      $conditions = ces::process_conditions($args['conditions']);
      if(str_replace('wage_', '', $args['select'][1]) != $args['select'][1]) {
        $conditions['industry'] = str_replace('wage_', '', $args['select'][1]);
        $select[] = 'wage_growth';
        if(isset($args['type']) && $args['type'] == 'by_state') {
          $args['order'] = 'region';
        }
      }
      if(str_replace('wage_', '', $args['select'][1]) != $args['select'][1]) {
        $conditions['industry'] = str_replace('wage_', '', $args['select'][1]);
        $select[] = 'wage_growth';
      } else {
        if (chart_info('type') == 'all') {
          $select[] = 'employment';
        } else {
          $select[] = 'employment_growth';
        }
        $conditions['industry'] = $args['select'][1];
      }

      if(in_array('data', $args['select'])) {
        $item_location = array_search('data', $args['select']);
        unset($args['select'][$item_location]);
        unset($args['select'][$item_location + 1]);
      }
      $select[] = 'region';
      $select[] = 'year';

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_qcew` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_qcew` <IF FIELD E> WHERE >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }

  
/*  public function advanced($args = null) {

  }*/

}

?>
