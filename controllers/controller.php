<?php

abstract class controller {

  public function index() {
    return false;
  }

  protected function process_conditions($condition_input) {
    $conditions = array();
    
    for($i = 0; $i < count($condition_input); $i += 2) {
      if(strpos($condition_input[$i], 'year') !== false) {
        if(!isset($conditions['year'])) {
          $conditions['year'] = array($condition_input[$i + 1]);
        } else {
          $conditions['year'][] = $condition_input[$i + 1];
        }
      } else {
        $conditions[$condition_input[$i]] = explode(',', $condition_input[$i + 1]);
      }
    }

    if(chart_info('type') == 'by_state' && !isset($conditions['region'])) {
      $conditions['region'] = array('CA','NY','TX');
    }
    return $conditions;
  }

  public function __construct() {
    require_once("models/" . $GLOBALS['request'][0] . "Model.php");
    $new = new $function();
    return $new;
  }

}

?>
