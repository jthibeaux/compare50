<?php

class income extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      $select = array();
      $select[] = chart_info('data');
      $select[] = 'region';
      $select[] = 'year';

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_income` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_income` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = @income::process_conditions($args['conditions']);

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = @$model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  public function advanced($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //Select
      $select = array();
      if($find = array_search('option', $args['select'])) {
        $select_item = $args['select'][0] . '_' . $args['select'][1];
        if ($args['select'][0] != 'differential') {
          $select_item .= '_' . $args['select'][$find + 1];
        }
        $select[] = $select_item;
      } else {
        $select[] = $args['select'][0] . '_' . $args['select'][1];
      }
      if($find = array_search('cpi', $args['select'])) {
        $select[] = 'cpi_' . $args['select'][$find + 1];
      }
      $select[] = 'region';
      $select[] = 'year';
      
      //TYPE ALL:
      //SELECT (`[FIELD A KEY]_[FIELD A VALUE]_[FIELD B VALUE]` <IF FIELD C>, `[FIELD C KEY]_[FIELD C VALUE]`<ENDIF>) FROM `data_income` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      //SELECT (`[FIELD A KEY]_[FIELD A VALUE]_[FIELD B VALUE]` <IF FIELD C>, `[FIELD C KEY]_[FIELD C VALUE]`<ENDIF>) FROM `data_income` <IF FIELD E> WHERE >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = array();
      for($i = 0; $i < count($args['conditions']); $i += 2) {
        if($args['conditions'][$i] == "start_year") {
          if (!isset($conditions['year<=']) || (isset($conditions['year<=']) && $conditions['year<='] >= $args['conditions'][$i + 1])) {
            $conditions['year>='] = $args['conditions'][$i + 1];
          } else {
            $conditions['year>='] = $conditions['year<='];
            if(isset($conditions['year<=']) && $conditions['year<='] < $args['conditions'][$i + 1]) {
              $conditions['year<='] = $args['conditions'][$i + 1];
            }
          }
        } elseif($args['conditions'][$i] == "end_year") {
          if (!isset($conditions['year>=']) || (isset($conditions['year>=']) && $conditions['year>='] <= $args['conditions'][$i + 1])) {
            $conditions['year<='] = $args['conditions'][$i + 1];
          } else {
            $conditions['year<='] = $conditions['year>='];
            if(isset($conditions['year>=']) && $conditions['year>='] > $args['conditions'][$i + 1]) {
              $conditions['year>='] = $args['conditions'][$i + 1];
            }
          }
        } else {
          $conditions[$args['conditions'][$i]] = $args['conditions'][$i + 1];
        }
      }
      if(!isset($args['order']) || $args['order'] == '') {
        $args['order'] = null;
      }
      $query = array(
        'table' => TABLE_PREFIX . __CLASS__,
        'select' => $select,
        'conditions' => $conditions,
        'order' => $args['order']
      );
    }
    
    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $JSON_function = 'format_JSON_' . $args['type'];
      $results = $JSON_function($call_function($query));
    }
    
    if (isset($results)) {
      return $results;
    } else {
      return false;
    }
  }  
}

?>
