<?php

class government extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      $select = array('value', 'region', 'year');

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_government` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_government` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = government::process_conditions($args['conditions']);
      $conditions['data'] = chart_info('data');

      if(!chart_info('region')) {
        $conditions['region'] = array('CA','NY','TX');
      }

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  public function advanced($args = null) {}  
}

?>
