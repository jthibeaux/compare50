<?php

include_once('earnings.php');

class disparity extends controller {

  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //Select
      $select = array('unemployment_rate', 'group', 'region', 'year');

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      $conditions = earnings::process_conditions($args['conditions']);
      
      $demo = chart_info('demographic');
      $location = array_search('demographic', $args['select']);
      unset($args['select'][$location + 1]);
      unset($args['select'][$location]);
      
      if($demo == 'gender') {
        $conditions['group'] = array('men', 'women');
      } else {
        $conditions['group'] = array(chart_info($demo . '1'), chart_info($demo . '2'));
      }

      if(in_array('data', $args['select'])) {
        $item_location = array_search('data', $args['select']);
        unset($args['select'][$item_location]);
        unset($args['select'][$item_location + 1]);
      }

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` <IF FIELD E> WHERE >= [FIELD E] AND <= [FIELD D]<ENDIF>

      $query = array(
        'table' => 'earnings',
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
/*  public function advanced($args = null) {

  }*/

  //Queries the DB and returns the results formatted properly for all-state graphs
  private function query_all($query_array, $special_lines = false) {
    $results = query_db($query_array);
  
    if(!$results) {
      return false;
    }
    $finished = array();
  
    $region = null;
    foreach($results as $result) {
      if ($region != $result['region']) {
        $finished[$result['region']] = array();
        $region = $result['region'];
      }
  
      for ($i = 0; $i < count($query_array['select']) - 2; $i++) {
         $finished[$result['region']][$query_array['select'][$i]] = $result[$query_array['select'][$i]];
      }
    }

    foreach ($finished as $state => $value) {
      if (!$special_lines) {
        $finished[$state] = array_shift($value);
      } else {
        if (!is_array($special_lines)) {
          $finished[$state] = $value[$special_lines];
        } else {
          unset($finished[$state]);
          foreach($special_lines as $special_line) {
              if(!isset($finished[$state])) {
                $finished[$state] = $value[$special_line];
              }
              $finished[$state] -= $value[$special_line];    
          }
          $finished[$state] = -$finished[$state];
        }
      }
    }
    asort($finished);
    return $finished;
  }
  
  //Queries the DB and returns the results formatted properly for by-state graphs
  private function query_by_state($query_array, $special_lines = false) {
    $results = query_db($query_array);
    
    if(!$results) {
      return false;
    }
    $finished = array();
  
    $region = null;
    foreach($results as $result) {
      if ($region != $result['region']) {
        $finished[$result['region']] = array();
        $region = $result['region'];
      }
  
      for ($i = 0; $i < count($query_array['select']) - 2; $i++) {
         $finished[$result['region']][$result['year']][$query_array['select'][$i]] = $result[$query_array['select'][$i]];
      }
    }

    foreach($finished as $state => $year_data) {
      foreach($year_data as $year =>$array) {
        $aggregate = 0;
        foreach($array as $item) {
          if ($aggregate == 0) {
            $aggregate += $item;
          } else {
            $aggregate -= $item;
          }
          $finished[$state][$year] = $aggregate;
        }
      }
      $unset = false;
    }

    return $finished;
  }

}

?>
