<?php

class demographics extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      $select = array('value', 'region', 'year');

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_demographics` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_demographics` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = demographics::process_conditions($args['conditions']);
      $conditions['data'] = chart_info('data');
      $data_sets = array('ethnicity', 'age', 'income', 'education', 'tenure', 'origin');
      $stat = $GLOBALS['request'][(array_search(chart_info('data'), $GLOBALS['request']) + 2)];
      $conditions['stat'] = $stat;

      if(!chart_info('region')) {
        $conditions['region'] = array('CA','NY','TX');
      }
      
      if(chart_info('values') == 'percent') {
        $sort = 'value DESC';
      } else {
        $sort = 'value ASC';
      }

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year', $sort)
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  public function advanced($args = null) {}  
}

?>
