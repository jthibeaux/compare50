<?php

class earnings extends controller {

  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //Select
      $select = array(str_replace('_gap', '', chart_info('statistic')) . '_real', 'group', 'region', 'year');

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      $conditions = earnings::process_conditions($args['conditions']);
      
      $demo = chart_info('demographic');
      $location = array_search('demographic', $args['select']);
      unset($args['select'][$location + 1]);
      unset($args['select'][$location]);
      
      if($demo == 'all') {
        $conditions['group']  = $demo;
      } else {
        if(in_array_match('gap', $args['select'])) {
          if($demo == 'gender') {
            $conditions['group'] = array('men', 'women');
          } else {
            $conditions['group'] = array(chart_info($demo . '1'), chart_info($demo . '2'));
          }
        } else {
          $conditions['group'] = chart_info($demo . '1');
        } 
      }

      if(in_array('data', $args['select'])) {
        $item_location = array_search('data', $args['select']);
        unset($args['select'][$item_location]);
        unset($args['select'][$item_location + 1]);
      }

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_earnings` <IF FIELD E> WHERE >= [FIELD E] AND <= [FIELD D]<ENDIF>

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }

    return $results;
  }
  
/*  public function advanced($args = null) {

  }*/

}

?>
