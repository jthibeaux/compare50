<?php

class employment extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_employment` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_employment` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $select = array();
      $conditions = employment::process_conditions($args['conditions']);
      if (chart_info('subset') == 'std') {
        $select[] = $args['select'][1] . '_std';
      } else {
        $select[] = $args['select'][1];
      }
      $select[] = 'region';
      $select[] = 'year';

      //TYPE ALL:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_employment` WHERE [FIELD G]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD A VALUE]_[FIELD L/FIELD K]` FROM `data_employment` <IF FIELD E> WHERE >= [FIELD E] AND <= [FIELD D]<ENDIF>

      $query = array(
        'table' => 'jobs',
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
/*  public function advanced($args = null) {

  }*/

}

?>
