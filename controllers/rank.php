<?php

class rank extends controller {

  public function index($args = null) {
                $args = $GLOBALS['request'];
                if($args[count($args) - 1] == 'unranked') {
                  unset($args[count($args) - 1]);
                }
                unset($args[0]);
                unset($args[1]);

                $select = array('value', 'region', 'year');

                //TYPE ALL:
                //SELECT `[FIELD L VALUE]` FROM `data_rank` WHERE [FIELD A]<ENDIF>
                //TYPE BY STATE:
                //SELECT `[FIELD L VALUE]` FROM `data_rank` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
                $priorities = array();
                $conditions = array();
                foreach($args as $arg) {
                  $pair = explode('+', $arg);
                  $conditions[] = '(`data` = "' . $pair[0] . '" AND `stat` = "' . $pair[1] . '")';
                  $priorities[] = array('data' => $pair[0], 'stat' => $pair[1]);
                }
                set('priorities', $priorities);

                $query = array(
                        'select' => '*',
                        'conditions' => '(' . implode(' OR ', $conditions) . ' ) AND (`year` = "2012")',
                        'order' => array('region', 'data', 'stat')
                );

                $model_name = __CLASS__ . 'Model';
                $results = $model_name::query_all($query);
        
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  public function advanced($args = null) {}  
}

?>
