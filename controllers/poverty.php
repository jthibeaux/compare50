<?php

class poverty extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      $select = array();
      $conditions = poverty::process_conditions($args['conditions']);
      if(str_replace('age_', '', $args['select'][1]) != $args['select'][1]) {
        $conditions['age_restricted'] = true;
        $select[] = str_replace('age_', '', $args['select'][1]);
        if(isset($args['type']) && $args['type'] == 'by_state') {
          $args['order'] = 'region';
        }
      } else {
        $conditions['age_restricted'] = null;
        $select[] = $args['select'][1];
      }
      $select[] = 'region';
      $select[] = 'year';

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_poverty` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_poverty` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }

}

?>
