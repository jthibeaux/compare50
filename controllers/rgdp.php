<?php

class rgdp extends controller {
  
  public function index($args = null) {
    if (isset($args['select']) && $args['select'] != null) {  
      $select = array('value', 'region', 'year');

      //TYPE ALL:
      //SELECT `[FIELD L VALUE]` FROM `data_rgdp` WHERE [FIELD A]<ENDIF>
      //TYPE BY STATE:
      //SELECT `[FIELD L VALUE]` FROM `data_rgdp` <IF FIELD E> WHERE [FIELD A] >= [FIELD E] AND <= [FIELD D]<ENDIF>
      $conditions = rgdp::process_conditions($args['conditions']);
      $conditions['data'] = chart_info('data');
                        
                        if(chart_info('data') == 'rgdp') {
                                $conditions['stat'] = chart_info('stat');
                        }

      if(!chart_info('region')) {
        $conditions['region'] = array('CA','NY','TX');
      }

      $query = array(
        'select' => $select,
        'conditions' => $conditions,
        'order' => array('region', 'year', 'value')
      );
    }

    if(isset($args['type']) && $args['type'] != '') {
      $call_function = 'query_' . $args['type'];
      $model_name = __CLASS__ . 'Model';
      $results = $model_name::$call_function($query);
    }
    
    if (!isset($results)) {
      return false;
    }
    return $results;
  }
  
  public function advanced($args = null) {}  
}

?>
