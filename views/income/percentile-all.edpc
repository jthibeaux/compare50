<!--Percentile Data Notes-->
<!--Long Term-->

The data are from March Current Population Survey (CPS) Annual Demographic files. All individual- or family-level data in the CPS are used with population weights. Income is in 2011 dollars, based on the CPI. The 10th percentile is the value such that 90 percent of observations have higher income, and 10 percent of observations have lower income. The median is the value such that 50 percent of observations have higher income, and 50 percent of observations have lower income. The 90th percentile is the value such that 10 percent of observations have higher income, and 90 percent of observations have lower income. The sample is restricted to families in which the family head is between the ages of 25-64 and is not self-employed. Family income growth is calculated as:
<blockquote>
<em>growth</em><sub><em>p,s,</em>1990-2010</sub> = (<em>income</em><sub><em>p,s,</em>2010</sub> - <em>income</em><sub><em>p,s,</em>1990</sub>)/<em>income</em><sub><em>p,s,</em>1990</sub>
</blockquote>
for percentile <em>p</em>, state <em>s</em> and years 1990 and 2010. Each bar represents the long-term growth in the income percentile for each state. The states are ordered from least growth to most growth, with California highlighted in red.