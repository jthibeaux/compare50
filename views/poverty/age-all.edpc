<!--Age Adjusted Poverty Rate Data Notes-->
<!--Long Term-->

<p>The data are from March Current Population Survey (CPS) Annual Demographic files. All individual- or family-level data in the CPS are used with population weights. The United States' average is calculated as unweighted averages over all states in each region. CPS March data are combined with Housing and Urban Development (HUD) Fair Market Rent (FMR) data to calculate the alternative poverty line. All poverty rates are defined at the family level. (Unlike the Census definition, families can consist of a single individual.) The poverty rate is calculated as the percentage of families in the CPS that are living below the nationally defined poverty thresholds, as indicated in the CPS March data.</p>

<!-- <p>The standardized poverty rate was calculated in several steps. First, five mutually exclusive race/ethnic categories were constructed: white (non-Hispanic), black (including Hispanics), Hispanic (white, Asian, other, but non-black), Asian (non-Hispanic), and other race (non-Hispanic). Families are categorized into each of the race/ethnicity groups if the head of the family is of that race or ethnicity. Second, the poverty rates were calculated for each race/ethnic group separately. At the same time, the share of the population made up of each race/ethnic group in each state in each year was calculated, as well as for the United States as whole in each year. The standardized poverty rates are calculated for each state as if all states had the exact same race/ethnic composition. To get the standard race/ethnic composition, we take the average of the proportion in each race/ethnic group in the United States as a whole over all years in the sample period (1990-2011). Defining these proportions as <em>____wgt</em>, the overall poverty rate as <em>poverty</em>, and, e.g., the poverty rate for whites as <em>white poverty</em>, the standardized poverty rates are calculated as:
<blockquote>
<em>standard poverty<sub>s,y</sub></em> = (<em>white poverty<sub>s,y</sub></em>x<em>whitewgt</em>) + (<em>Hispanic poverty<sub>s,y</sub></em>x<em>hispanwgt</em>) + (<em>black poverty<sub>s,y</sub></em>x<em>blackwgt</em>) + (<em>Asian poverty<sub>s,y</sub></em>x<em>asianwgt</em>) + (<em>other poverty<sub>s,y</sub></em>x<em>otherwgt</em>).
</blockquote>
For example, the standardized poverty rate is lower in California than the actual poverty rate because California has a relatively larger Hispanic population than the nation as a whole, and the Hispanic population has a relatively higher poverty level. </p> -->

The alternative poverty rate is based on a poverty threshold that varies by state according to housing costs (Citro and Michael, 1995). The alternative poverty threshold is created by scaling the national poverty thresholds defined by the Census Bureau by a housing index that is equal to 1 if the housing costs in the state are equal to the national average. The housing index is calculated using the HUD FMR data from￼￼ 1990-2010. The HUD calculates a separate FMR for each rural and urban entity in the state. For each state, we calculated the mean FMR for rural and urban areas separately using the HUD data. Using the CPS March Data, we calculated rural and urban population shares for each state, such that the percentage of the population living in rural areas plus the percentage of the population living in the urban areas is equal to the total population in the state. We calculate the overall state FMR as
<blockquote>
<em>FMR<sub>s,y</sub></em> = (<em>rural FMR</em>)<em><sub>s,y</sub></em>x(<em>rural population share</em>)<em><sub>s,y</sub></em> + (<em>urban FMR</em>)<em><sub>s,y</sub></em>x(<em>urban population share</em>)<em><sub>s,y</sub></em>
</blockquote>
for each state <em>s</em> and year <em>y</em>. We then calculate the national average FMR for each year by averaging all the state FMRs. The housing index for each state is equal to
<blockquote>
(<em>FMR<sub>s,y</sub></em>)/(<em>national average FMR<sub>y</sub></em>).
</blockquote>
According to (Citro and Michael, 1995), about 44 percent of the poverty budget is devoted to housing; we therefore downscale the housing index accordingly: 
<blockquote>
(<em>adjusted index<sub>s,y</sub></em>) = (<em>index<sub>s,y</sub></em> - 1)x0.44 + 1.
</blockquote>
Finally, we multiplied the nationally defined poverty thresholds for each family in the CPS March Sample by the corresponding state index to create the new alternative poverty threshold. The alternative poverty rate is calculated as the percentage of families with family income below the alternative poverty threshold.</p>

<p>Each bar in the figure represents a percentage point change in the poverty rate for each state. The change in poverty is equal to (<em>Poverty Rate<sub>2011</sub></em> – <em>Poverty Rate<sub>1990</sub></em>). The states are ordered from largest decrease to largest increase in poverty, with California highlighted in red.</p>