<?php
  $sections = array(
    'Economy' => array(
      array('link'=>'growth', 'text' => 'Economic Growth', 'match'=>array('growth', 'ces', 'qcew'), 
        'graphs' =>array(
          'real_gsp_percapita_growth' => array(
            'about' => "This chart displays the total value of a state’s economy.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'real_gsp_growth' => array(
            'about' => "This chart displays the annual percent of GSP growth.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'gsp_labors_share' => array(
            'about' => "This chart displays the labor force’s share of a state’s economy.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'all' => array(
            'about' => "This chart displays the annual percent of employment growth for all industries in the private and public sectors.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'all_private' => array(
            'about' => "This chart displays the annual percent of employment growth for all industries in the private sector.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'manufacturing_all_private' => array(
            'about' => "This chart displays the annual percent of employment growth for manufacturing industries in the private sector.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'tech' => array(
            'about' => "This chart displays the annual percent of employment growth for non-farm industries using CES data.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'total_nonfarm' => array(
            'about' => "This chart displays the annual percent of employment growth for all industries in the private sector using CES data.", 
            'sources' => "Bureau of Labor Statistics Current Employment Statistics (CES)"
          ),
          'total_private' => array(
            'about' => "This chart displays the annual percent of employment growth for all industries in the private sector using CES data.", 
            'sources' => "Bureau of Labor Statistics Current Employment Statistics (CES)"
          ),
          'wage_all' => array(
            'about' => "This chart displays the annual percent of growth for wages for all industries.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'wage_all_private' => array(
            'about' => "This chart displays the annual percent of growth for wages for all industries in the private sector.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'wage_manufacturing_all_private' => array(
            'about' => "This chart displays the annual percent of growth for wages for all industries in the private sector.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          )
        )
      ),
      array('link'=>'housing', 'text'=> 'Housing', 'match'=> array('housing'),
        'graphs' => array(
          'units' => array(
            'about' => "This chart displays the number of permits for new residential housing by year.", 
            'sources' => "U.S. Census Bureau"
          ),
          'values' => array(
            'about' => "This chart displays the total value of new residential housing.", 
            'sources' => "U.S. Census Bureau"
          )
        )
      ),
      array('link'=>'rgdp', 'text'=>'Real GDP', 'match' => array('rgdp'), 
        'graphs' => array(
          'rgdp' => array(
            'about' => "This chart displays the Real GDP in a state by industry sector.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'per_capita' => array(
            'about' => "This chart displays the Real GDP in a state by worker.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          )
        )
      ),
      array('link'=>'government', 'text'=>'Government', 'match' => array('government'),
        'graphs' => array(
          'revenues' => array(
            'about' => "This chart displays the total amount of revenue received by a state.", 
            'sources' => "U.S. Census Bureau"
          ),
          'expenditures' => array(
            'about' => "This chart displays a state’s total expenditures.", 
            'sources' => "U.S. Census Bureau"
          ),
          'revenues_per_capita' => array(
            'about' => "This chart displays the total amount of revenue received by a state per capita.", 
            'sources' => "U.S. Census Bureau"
          ),
          'expenditures_per_capita' => array(
            'about' => "This chart displays a state’s total expenditures per capita.", 
            'sources' => "U.S. Census Bureau"
          )
        )
      ),
      'about_section' => 'This section presents economic indicators for all 50 states.  Economic growth is the increase in the amount of the goods and services produced by an economy over time. It is conventionally measured as the percent rate of increase in real gross state product, or real GSP. In economics, "economic growth" typically refers to growth of potential output, i.e., production at "full employment," which is caused by growth in aggregate demand or observed output. We also track indicators relating to housing in this section as well as data on state government spending and revenue.'
    ),
    'Innovation' => array(
      array('link'=>'innovation', 'text'=>'Innovation', 'match' => array('innovation'),
        'graphs' => array(
          'exports' => array(
            'about' => "This chart displays the total value of exports from a state.", 
            'sources' => "WiserTrade"
          ),
          'capital' => array(
            'about' => "This chart displays the total amount of private venture capital investment flowing into a state.", 
            'sources' => "PwC MoneyTree"
          ),
          'patents' => array(
            'about' => "This chart displays the total number of patents granted by state.", 
            'sources' => "U.S. Patent and Trademark Office"
          )
        )
      ),
      'about_section' => 'Innovation plays an important role in how the state’s economy performs over time. This section tracks key innovation indicators, including patents, venture capital investment, and the value of exports.'
    ),
    'Jobs' => array(
      array('link'=>'employment', 'text' => 'Employment', 'match'=>array('employment', 'hhemp', 'qcewreport'),
        'graphs' => array(
          'emp_to_pop' => array(
            'about' => "This chart displays the ratio of the working age population that is employed.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'underemployed' => array(
            'about' => "This chart displays the percent of the working population whose current position is under what they are qualified for.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'mean_hours' => array(
            'about' => "This chart displays the mean weekly hours people are working.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'labor_force' => array(
            'about' => "This chart displays the total labor force by state over time.", 
            'sources' => "U.S. Bureau of Labor Statistics"
          ),
          'emp' => array(
            'about' => "This chart displays the total number of households where one or more members of the household is employed.", 
            'sources' => "U.S. Bureau of Labor Statistics"
          ),
          'employment' => array(
            'about' => "This chart displays the total number of people with legal employment.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'stat' => array(
            'about' => "This chart displays the total employment by industry sector.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'establishments' => array(
            'about' => "This chart displays the total number of firms .", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          )
        )
      ),
      array('link'=>'unemployment', 'text'=>'Unemployment', 'match' => array('unemployment', 'hhunemp', 'disparity'),
        'graphs' => array(
          'unemployment' => array(
            'about' => "This chart displays the unemployment rate by state over time.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'unemp' => array(
            'about' => "This chart displays the total number of households where one or more members of the household is unemployed.", 
            'sources' => "U.S. Bureau of Labor Statistics"
          ),
          'nilf_disc' => array(
            'about' => "This chart displays the percent of the working age population who not employed and are not seeking employment .", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'mean_unemp' => array(
            'about' => "This chart displays the median number of weeks people are unemployed for.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'disparity' => array(
            'about' => "This chart displays the difference in unemployment rates by gender and ethnicity.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          )
        )
      ),
      array('link'=>'layoffs', 'text'=>'Separations', 'match' => array('layoffs'),
        'graphs' => array(
          'private' => array(
            'about' => "This chart displays the increased percent of people who left their job this year compared to last year, voluntarily or involuntarily.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'business' => array(
            'about' => "This chart displays the increased percent of people who left their job for business reasons this year compared to last year, voluntarily or involuntarily.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'financial' => array(
            'about' => "This chart displays the increased percent of people who left their job for financial reasons this year compared to last year, voluntarily or involuntarily.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          )
        )
      ),
      'about_section' => 'Job growth, layoffs, and unemployment are all represented in this category. These are measures of economic performance that closely track output, and also are of great importance to policymakers and state residents. One of the major factors that leads to sharp increases in unemployment during a recession is mass layoffs, where firms close or downsize sharply.'
    ),
    'Income & Equity' => array(
      array('link'=>'income', 'text' => 'Income Distribution', 'match'=>array('income'),
        'graphs' => array(
          'percentile_10_adj' => array(
            'about' => "This graph displays the total family income for the lowest 10 percent of the population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'percentile_25_adj' => array(
            'about' => "This graph displays the total family income for the lowest 25 percent of the population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'percentile_50_adj' => array(
            'about' => "This graph displays the medial total family income.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'percentile_75_adj' => array(
            'about' => "This graph displays the total family income for the highest 75 percent of the population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'percentile_90_adj' => array(
            'about' => "This graph displays the total family income for the highest 90 percent of the population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'differential_50-10' => array(
            'about' => "This graph displays the difference between the lowest 10 percent of the population and the median.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'differential_90-50' => array(
            'about' => "This graph displays the difference between the highest 90 percent of the population and the median.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'differential_90-10' => array(
            'about' => "This graph displays the difference between the lowest 10 percent of the population and the highest 90 percent.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          )
        )
      ),
      array('link'=>'earnings', 'text' => 'Earnings', 'match'=>array('earnings', 'qcewearnings'),
        'graphs' => array(
          'median_weekly' => array(
            'about' => "This graph displays the mean amount of weekly earnings for a state’s population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'median_weekly_gap' => array(
            'about' => "This graph displays the difference in weekly earnings by gender and ethnicity for a state’s population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'median_hourly' => array(
            'about' => "This graph displays the mean amount of hourly earnings for a state’s population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'median_hourly_gap' => array(
            'about' => "This graph displays the difference in hourly earnings by gender and ethnicity for a state’s population.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'payroll' => array(
            'about' => "This graph displays the total dollar of payroll for all employment.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          ),
          'wages' => array(
            'about' => "This graph displays the average annual wage for all employment.", 
            'sources' => "U.S. Bureau of Labor Statistics Quarterly Census of Employment (QCEW)"
          )
        )
      ),
      array('link'=>'poverty', 'text' => 'Poverty', 'match'=>array('poverty'),
        'graphs' => array(
          'poverty_rate' => array(
            'about' => "This graph displays the percentage of the state’s population, including children and seniors, that are living in poverty.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'poverty_rate_adj' => array(
            'about' => "This graph displays the percentage of the state’s population, including children and seniors, that are living in poverty, adjusted for the cost of living.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'age_poverty_rate' => array(
            'about' => "This graph displays the percentage of the state’s working age population that are living in poverty.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          ),
          'age_poverty_rate_adj' => array(
            'about' => "This graph displays the percentage of the state’s working age population that are living in poverty, adjusted for the cost of living.", 
            'sources' => "U.S. Census Bureau Current Population Survey"
          )
        )
      ),
      array('link'=>'personal', 'text'=>'Personal Income', 'match' => array('personal'),
        'graphs' => array(
          'personal_income' => array(
            'about' => "This chart displays the total value of all personal income received.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'per_capita' => array(
            'about' => "This chart displays the total value of all personal income received per capita.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'wage_salary' => array(
            'about' => "This chart displays the wage and salary income by state.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          ),
          'business_income' => array(
            'about' => "This chart displays the proprietors’ income by state.", 
            'sources' => "U.S. Bureau of Economic Analysis (BEA)"
          )
        )
      ),
      'about_section' => "In this section we are measuring incomes at the family level and are looking at total family income, which determines the economic resources available to that family.<br><br>Because a state's economic performance should be assessed not only on how many jobs it produces, and for whom, but on the level of earnings in those jobs, we explore earnings in detail in this section. Higher earnings (as well as higher employment) imply that individuals and families have access to more economic resources, which, in addition to helping them, is beneficial to state finances, with higher tax revenue and less dependency on public support.<br><br>The section also explores the poverty rate and measures the fraction of families below some predetermined level of income needed to satisfy a given level of needs."
    ),
    'Demographics' => array(
      array('link'=>'population', 'text'=>'Population', 'match' => array('population'),
        'graphs' => array(
          'population' => array(
            'about' => "This chart displays the total population by state.", 
            'sources' => "U.S. Census Bureau"
          ),
          'stat' => array(
            'about' => "This chart displays various changes to the population, such as births, deaths, and net migration.", 
            'sources' => "U.S. Census Bureau"
          )
        )
      ),
      array('link'=>'demographics', 'text'=>'Community', 'match' => array('demographics'),
        'graphs' => array(
          'ethnicity' => array(
            'about' => "This chart displays the race/ethnicity levels in a state as a percentage of the state or in total.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          ),
          'age' => array(
            'about' => "This chart displays the age levels in a state as a percentage of the state or in total.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          ),
          'income' => array(
            'about' => "This chart displays household income levels and the percentage of the population or the number of people that fall above those levels.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          ),
          'education' => array(
            'about' => "This chart displays education levels and the percentage of the population or the number of people at those levels.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          ),
          'tenure' =>array(
            'about' =>  "This chart displays the percentage of the population or total people that rent or own their properties.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          ),
          'origin' => array(
            'about' => "This chart displays the country of origin by percentage of the population or number of people.", 
            'sources' => "U.S. Census Bureau American Community Survey"
          )
        )
      ),
      'about_section' => "This section looks at demographic data for all states from the U.S. Census."
    )
  );
  
  set('sections', $sections);
?>
