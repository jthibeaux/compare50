<?php

//Name of the site
define("SITE_NAME", '');

//Base URLs of the site
define("BASE_URL", '');
define("REPORT_URL", "");

//Database connection info
define("DB_USER", '');
define("DB_PASS", '');
define("DB_NAME", '');
define("DB_HOST", '');
define("TABLE_PREFIX", 'data_');

//Layout
define("LAYOUT", 'default');
define("HOMEPAGE", 'pages/home.edpc');

//Data
define("START_YEAR", 1990);
define("END_YEAR", 2011);
define("HIGHLIGHTED", 'CA');

//Settings
define('TEST_MODE', false);

?>
