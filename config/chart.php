<?php
if (explode('/', substr($_SERVER["REQUEST_URI"], 1)) != $GLOBALS['request']) {
  $view_name = $GLOBALS['view_name'];
  $original_start_year = chart_info('start_year', explode('/', substr($_SERVER["REQUEST_URI"], 1)));
  $original_end_year = chart_info('end_year', explode('/', substr($_SERVER["REQUEST_URI"], 1)));
  $start_year = $GLOBALS['start_year'];
  $end_year = $GLOBALS['end_year'];
  
  if($original_start_year < $start_year) {
    $view_name = str_replace($original_start_year, $start_year, $view_name);
  } 
  if($original_end_year < $end_year) {
    $view_name = str_replace($original_end_year, $end_year, $view_name);
  }
  set('view_name', $view_name);
}

$type = chart_info('type');
$chart = null;
if ($type == 'by_state') {
$chart = "
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
              backgroundColor: '#f5f7f7',
                renderTo: 'container',
                type: 'line',
                marginRight: 130,
                marginBottom: 50,
                marginTop: 50,
            },
            credits: {
              enabled: false
            },
            title: {
                text: \"" . $GLOBALS['view_name'] . "\",
                x: -20, //center
                style: {width: '750px'}
            },
            subtitle : {
              text: \"" . render_sources() . "\",
              y: 400
            },
            xAxis: {
                categories: " . $GLOBALS['chart_data']['categories'] . "
            },
            yAxis: {
                title: {
                    text: '" . $GLOBALS['y-axis'] . "'
                },
                labels: {
                  formatter: function() {
                    return '" . $GLOBALS['data_pre'] . "' + tickIntervals(this.value) + '" . $GLOBALS['data_post'] . "';
                  }
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': " . $GLOBALS['data_pre'] . "'+ addCommas(this.y) + '" . $GLOBALS['data_post'] . "';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [" . $GLOBALS['chart_data']['data'] . "]
        });
    });
    
});";

} else {
$chart = "
$(function () {
    var chart;
    $(document).ready(function() {
    
        var colors = Highcharts.getOptions().colors,
            categories = " . $GLOBALS['chart_data']['categories'] . ",
            name = 'States',
            data = [" . $GLOBALS['chart_data']['data'] . "];
    
        function setChart(name, categories, data, color) {
      chart.xAxis[0].setCategories(categories, false);
      chart.series[0].remove(false);
      chart.addSeries({
        name: name,
        data: data,
        color: color || 'white'
      }, false);
      chart.redraw();
        }
    
        chart = new Highcharts.Chart({
            chart: {
              backgroundColor: '#f5f7f7',
                renderTo: 'container',
                type: 'column',
                style: {
                  fontFamily: '\"Helvetica Neue\", Helvetica, Arial, sans-serif'
                },
                marginTop: 50,
                marginBottom: 125
            },
            credits: {
              enabled: false
            },
            legend: {
              enabled: false
          },
            title: {
                text: \"" . $GLOBALS['view_name'] . "\",
                style: {width: '750px'}
            },
            subtitle : {
              text: \"" . render_sources() . "\",
              y: 400
            },
            xAxis: {
                categories: categories,
                title: {
                    text: 'States'
                },
                labels: {
                  rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: '" . $GLOBALS['y-axis'] . "'
                },
                labels: {
                  formatter: function() {
                    return '" . $GLOBALS['data_pre'] . "' + tickIntervals(this.value) + '" . $GLOBALS['data_post'] . "';
                  }
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                              var point = this.point,
                              t = this.color;
                var index = 0;
                var string = '';
                              for (i = 0; i < colors.length; i++) {
                                if (t == colors[i]) {
                                  index = i;
                                  i = colors.length;
                                }
                              }
                              if (index == colors.length - 1) {
                                index = -1;
                              }

                              this.update({
                            color: colors[index + 1]
                          });
                            }
                        }
                    },
                    dataLabels: {
                        enabled: false,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return '" . $GLOBALS['data_pre'] . "'+ addCommas(this.y) + '" . $GLOBALS['data_post'] . "';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                        s = this.x +': " . $GLOBALS['data_pre'] . "'+  addCommas(this.y) + '" . $GLOBALS['data_post'] . "';
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }]
        });
    });
    
});

";

} 

set('chart', $chart);

?>
