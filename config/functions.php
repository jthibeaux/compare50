<?php 

//--------------------------------------------- BASIC FUNCTIONS -------------------------------------------//

/*******************************************************************************************************
  convert_state function written by Sean Barton
  http://www.sean-barton.co.uk/2008/05/converting-us-state-names-into-their-appreviations-and-back/
  May 24th, 2008
*******************************************************************************************************/
function convert_state($name, $to = 'name') {
  $states = array(
  array('name'=>'Alabama', 'abbrev'=>'AL'),
  array('name'=>'Alaska', 'abbrev'=>'AK'),
  array('name'=>'Arizona', 'abbrev'=>'AZ'),
  array('name'=>'Arkansas', 'abbrev'=>'AR'),
  array('name'=>'California', 'abbrev'=>'CA'),
  array('name'=>'Colorado', 'abbrev'=>'CO'),
  array('name'=>'Connecticut', 'abbrev'=>'CT'),
  array('name'=>'Washington DC', 'abbrev' => 'DC'),
  array('name'=>'Delaware', 'abbrev'=>'DE'),
  array('name'=>'Florida', 'abbrev'=>'FL'),
  array('name'=>'Georgia', 'abbrev'=>'GA'),
  array('name'=>'Hawaii', 'abbrev'=>'HI'),
  array('name'=>'Idaho', 'abbrev'=>'ID'),
  array('name'=>'Illinois', 'abbrev'=>'IL'),
  array('name'=>'Indiana', 'abbrev'=>'IN'),
  array('name'=>'Iowa', 'abbrev'=>'IA'),
  array('name'=>'Kansas', 'abbrev'=>'KS'),
  array('name'=>'Kentucky', 'abbrev'=>'KY'),
  array('name'=>'Louisiana', 'abbrev'=>'LA'),
  array('name'=>'Maine', 'abbrev'=>'ME'),
  array('name'=>'Maryland', 'abbrev'=>'MD'),
  array('name'=>'Massachusetts', 'abbrev'=>'MA'),
  array('name'=>'Michigan', 'abbrev'=>'MI'),
  array('name'=>'Minnesota', 'abbrev'=>'MN'),
  array('name'=>'Mississippi', 'abbrev'=>'MS'),
  array('name'=>'Missouri', 'abbrev'=>'MO'),
  array('name'=>'Montana', 'abbrev'=>'MT'),
  array('name'=>'Nebraska', 'abbrev'=>'NE'),
  array('name'=>'Nevada', 'abbrev'=>'NV'),
  array('name'=>'New Hampshire', 'abbrev'=>'NH'),
  array('name'=>'New Jersey', 'abbrev'=>'NJ'),
  array('name'=>'New Mexico', 'abbrev'=>'NM'),
  array('name'=>'New York', 'abbrev'=>'NY'),
  array('name'=>'North Carolina', 'abbrev'=>'NC'),
  array('name'=>'North Dakota', 'abbrev'=>'ND'),
  array('name'=>'Ohio', 'abbrev'=>'OH'),
  array('name'=>'Oklahoma', 'abbrev'=>'OK'),
  array('name'=>'Oregon', 'abbrev'=>'OR'),
  array('name'=>'Pennsylvania', 'abbrev'=>'PA'),
  array('name'=>'Rhode Island', 'abbrev'=>'RI'),
  array('name'=>'South Carolina', 'abbrev'=>'SC'),
  array('name'=>'South Dakota', 'abbrev'=>'SD'),
  array('name'=>'Tennessee', 'abbrev'=>'TN'),
  array('name'=>'Texas', 'abbrev'=>'TX'),
  array('name'=>'Utah', 'abbrev'=>'UT'),
  array('name'=>'Vermont', 'abbrev'=>'VT'),
  array('name'=>'Virginia', 'abbrev'=>'VA'),
  array('name'=>'Washington', 'abbrev'=>'WA'),
  array('name'=>'West Virginia', 'abbrev'=>'WV'),
  array('name'=>'Wisconsin', 'abbrev'=>'WI'),
  array('name'=>'Wyoming', 'abbrev'=>'WY'),
  array('name'=>'United States', 'abbrev'=>'US'),
  array('name'=>'US Average*', 'abbrev'=>'USA'),
  array('name'=>'Pacific Division', 'abbrev'=>'PD'),
  array('name'=>'West Region', 'abbrev'=>'WR'),
  //array('name'=>'West (no CA)', 'abbrev'=>'WR-CA'),
  //array('name'=>'Pacific (no CA)', 'abbrev'=>'PD-CA'),
  //array('name'=>'United States (no CA)', 'abbrev'=>'US-CA'),
  array('abbrev' => 'asian', 'name' => 'Asian'),
  array('abbrev' => 'black', 'name' => 'Black'),
  array('abbrev' => 'hispanic', 'name' => 'Hispanic'),
  array('abbrev' => 'other', 'name' => 'Other'),
  array('abbrev' => 'white', 'name' => 'White'),
  array('abbrev' => 'white_nh', 'name' => 'White, non-Hispanic'),
  array('abbrev' => 'AK_HI_PRi', 'name' => 'Alaska/Hawaii/Puerto Rico'),
  array('abbrev' => 'DCi', 'name' => 'DC Metro'),
  array('abbrev' => 'LA_OCi', 'name' => 'LA/Orange County'),
  array('abbrev' => 'MWi', 'name' => 'Midwest'),
  array('abbrev' => 'NEi', 'name' => 'New England'),
  array('abbrev' => 'NYi', 'name' => 'New York Metro'),
  array('abbrev' => 'NCi', 'name' => 'North Central'),
  array('abbrev' => 'NWi', 'name' => 'Northwest'),
  array('abbrev' => 'PAi', 'name' => 'Philadelphia Metro'),
  array('abbrev' => 'norcali', 'name' => 'Sacramento/Northern California'),
  array('abbrev' => 'SDi', 'name' => 'San Diego'),
  array('abbrev' => 'SVi', 'name' => 'Silicon Valley'),
  array('abbrev' => 'SCi', 'name' => 'South Central'),
  array('abbrev' => 'SEi', 'name' => 'Southeast'),
  array('abbrev' => 'SWi', 'name' => 'Southwest'),
  array('abbrev' => '??', 'name' => 'Unknown'),
  array('abbrev' => 'domestic', 'name' => 'Domestic'),
  array('abbrev' => 'ENC', 'name' => 'East_North_Central'),
  array('abbrev' => 'ESC', 'name' => 'East_South_Central'),
  array('abbrev' => 'MDA', 'name' => 'Middle_Atlantic'),
  array('abbrev' => 'MDW', 'name' => 'Midwest'),
  array('abbrev' => 'MTN', 'name' => 'Mountain'),
  array('abbrev' => 'NE', 'name' => 'Northeast'),
  array('abbrev' => 'PAC', 'name' => 'Pacific'),
  array('abbrev' => 'SA', 'name' => 'South_Atlantic'),
  array('abbrev' => 'W', 'name' => 'West'),
  array('abbrev' => 'WNC', 'name' => 'West_North_Central'),
  array('abbrev' => 'WSC', 'name' => 'West_South_Central')
  );

  $return = false;
  foreach ($states as $state) {
    if ($to == 'name') {
      if (strtolower($state['abbrev']) == strtolower($name)){
        $return = $state['name'];
        break;
      }
    } else if ($to == 'abbrev') {
      if (strtolower($state['name']) == strtolower($name)){
        $return = strtoupper($state['abbrev']);
        break;
      }
    }
  }
  
  if(!$return) {
    $return = ucfirst($name);
  }
  return $return;
}

function chart_alter($item, $value) {
  $request = $GLOBALS['request'];
  $request[array_search($item, $request) + 1] = $value;
  set ("request", $request);
}

function chart_info($item, $request = false) {
  if (!isset($request) || !$request) {
    $request = $GLOBALS['request'];
  }
  if (is_array($request) && isset($GLOBALS['default_request']) && is_array($GLOBALS['default_request'])) {
    if (count($request) > 2 && in_array($item, $request)) {
      return $request[array_search($item, $request) + 1];
    } elseif (!in_array($item, $GLOBALS['default_request']) && !in_array($item, $request)) {
      return false;
    } else {
      return $GLOBALS['default_request'][array_search($item, $GLOBALS['default_request']) + 1];
    }
  }
}

function debug($array = 'ohai') {
  if(TEST_MODE) {
    echo '<pre>' . print_r($array, true) . '</pre><br>';
  } else {
    trigger_error(print_r($array, true));
  }
}

function effectively_empty($array) {
  foreach($array as $item) {
    if($item != '') {
      return false;
    }
  }
  return true;
}

function in_array_match($part, $array) {
    if (is_array($array)) {
      foreach ($array as $key => $v) {
          $match = strpos($v, $part);
          if (is_numeric($match)) {
              return $key;
          }
      }
    }
    return false;
}

function is_state($state_abbreviation) {
  if (convert_state($state_abbreviation) != $state_abbreviation) {
    return true;
  } else {
    return false;
  }
}

/************************************************************
  query_db
  @param $options An array of options for the query
    $options An array of options which dictate how the 
  @return An array of strings, ints, doubles, etc 
    representing the results of the query
*************************************************************/
function query_db($options = NULL, $type_override = false) {
  debug($options);

  if (is_array($options)) {
    
    $query_array = array(
      'table' => '',
      'unique' => '',
      'order' => '',
      'select' => '',
      'conditions' => ''
    );
    if (!isset($options['table'])) {
      $options['table'] = '';
    }
    foreach($query_array as $query_element => $blank_placeholder) {
      if(isset($options[$query_element])) {
        $function_name = 'query_format_' . $query_element;
        $query_array[$query_element] = $function_name($options[$query_element], $type_override);
      } else {
        $query_array[$query_element] = '';
      }
    }

    global $database;
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  
    if ($mysqli->connect_error) {
        debug('Connect Error: ' . $mysqli->connect_error);
    }

    $query_string = "SELECT " . $query_array['unique'] . $query_array['select'] . " FROM `" . $query_array['table'] . "`" . $query_array['conditions'] . $query_array['order'] . ';';

    if ($result = $mysqli->query($query_string)) {
        debug('Query succeeded: ' . $query_string);
                    $result_array = array();
        while($result_array[] = $result->fetch_assoc()){} 
            array_pop($result_array);
        $result->close();
    } else {
      debug('Query failed: ' . $query_string);
      return false;
    }
    $mysqli->close();
    
    return $result_array;
  } else {
    debug('Options not indicated');
    return false;
  }
}

function query_format_conditions($condition_input = null, $type_override = false) {
        if(is_numeric($condition_input)) {
          return null;
        }
  if(isset($condition_input) && !is_array($condition_input)) {
          $conditions = ' WHERE ' . $condition_input;
        } else {
    $conditions = array();
    foreach ($condition_input as $field_name => $required_value) {
      $field_array = explode(',', $field_name);
      $field_name = array_shift($field_array);
      
      if (is_array($required_value)) {
        asort($required_value);
        if ($field_name == 'year') {
          if(chart_info('type') == 'all' && !$type_override) {
            $conditions[] = '(`year` = ' . $required_value[0] . ' OR `year` = ' . $required_value[1] . ')';
          } else {
            $conditions[] = '(`year` >= ' . $required_value[0] . ' AND `year` <= ' . $required_value[1] . ')';
          }
        } else {
          $conditions[] = "(`" . $field_name . "` = '" . implode("' OR `" . $field_name . "` = '", $required_value) . "')";
        }
      } else {
        if(strpos($field_name, '!=') !== FALSE && $required_value == 'NULL') {
          $conditions[] = "`" . substr($field_name, 0, -2) . '` IS NOT NULL';
        } elseif(strpos($field_name, '!=') !== FALSE) {
          $conditions[] = "`" . substr($field_name, 0, -2) . "` != '" . $required_value . "'";
        } else {
          $conditions[] = "`" . $field_name . "` = '" . $required_value . "'";
        }
      }
    }
          $conditions = ' WHERE ' . implode(' AND ', $conditions);
  }
  
  return $conditions;
}

function query_format_order($order_input = null, $type_override = false) {
  if(isset($order_input) && $order_input != '' && $order_input != null) {
    if(is_numeric($order_input)) {
      $order_input = " ORDER BY `" . $options['select'][$order_input - 1] . '`';
    } elseif (is_array($order_input)) {
      $order = " ORDER BY ";
      foreach($order_input as $order_item) {
        $order .= '`' . str_replace(' ASC', '', str_replace(' DESC', '', $order_item)) . '`';
        if(strpos($order_item, ' DESC')) {
          $order .= ' DESC';
        } else {
          $order .= ' ASC';
        }
        $order .=  ', ';
        $order_input = substr($order, 0 ,-2);
      }
    } else {
      $order = " ORDER BY ";
      $order .= '`' . str_replace(' ASC', '', str_replace(' DESC', '', $order_input) . '`');
      if(strpos($order_input, ' DESC')) {
        $order .= ' DESC ';
      } else {
        $order .= ' ASC ';
      }
      $order_input = $order;
    }
  } else {
    $order_input = '';
  }
  
  return $order_input;
}

function query_format_select($select_input = null, $type_override = false) {
  $not_empty = '';
  if(!isset($select_input) || $select_input == '*') {
    $select_input = '*';
  } elseif(!is_array($select_input)) {
    if ((substr($select_input, 0, 4) == 'MAX(') || (substr($select_input, 0, 4) == 'MIN(')) {
      $not_empty .= '`' . substr($select_input, 4, -1) . '` != \'\' AND ';
    } else {
      $not_empty = '`' . $select_input . '` != \'\'';
      $select_input = '`' . $select_input . '`';
    }
  } else {
    $select = '';
    foreach($select_input as $select_item) {
      $select .= '`' . $select_item . '`, ';
      $not_empty .= '`' . $select_item . '` != \'\' AND ';
    }
    $select_input = substr($select, 0, -2);
    $not_empty = substr($not_empty, 0, -5);
  }
  return $select_input;
}

function query_format_table($table_input = null, $type_override = false) {
  if(!isset($table_input) || $table_input == '') {
            if(isset($GLOBALS['request'])) {
              return TABLE_PREFIX . $GLOBALS['request'][0];
            } else { 
              return TABLE_PREFIX . $GLOBALS['default_request'][0];
            }
  } else {
    return TABLE_PREFIX . $table_input;
  }
}

function query_format_unique($unique_input = null, $type_override = false) {
  if(isset($unique_input) && $unique_input) {
    return ' DISTINCT ';
  } else {
    return '';
  }
}

function set($variable_name, $variable_value) {
  $variable = $variable_name;
  global $$variable;
  $$variable = $variable_value;
}

//---------------------------------------- FORM-GENERATION FUNCTIONS --------------------------------------//

function generate_check($views_options_item, $views_option, $options = null) {}

function generate_hidden($views_options_item, $views_option, $request = null) {
  $input = '<input type="hidden" class="' . $views_option . '" name="' . $views_options_item['placement'] . '[' . $views_option . ']" value="' . $views_options_item['value'] . '">';
  return array('item' => $input);
}

function generate_multiselect($views_options_item, $views_option, $request = null) {
  $views_option_class = str_replace(',', ' ', $views_option) . ' ' . $views_option;

  if(isset($request) && is_array($request) && !empty($request)) {
    $options = array();
  }
  for($i = 2; $i < count($request); $i += 2) {
    $options[$request[$i]] = $request[($i + 1)];
  }
  $multiselect = array();
  if(count($views_options_item) > 1) {
    if (isset($views_options_item['placement']) && $views_options_item['placement'] != '') {
      $field_name = $views_options_item['placement'];
      unset($views_options_item['placement']);
    } else {
      $field_name = 'field';
    }
    if (isset($views_options_item['type']) && $views_options_item['type'] != '') {
      unset($views_options_item['type']);
    }
    if (isset($views_options_item['label']) && $views_options_item['label'] != '') {
      $label = '<label class="' . $views_option_class  . '">' . $views_options_item['label'] . '</label>';
      unset($views_options_item['label']);
    } else {
      $label = null;
    }
    $multiselect_name = $field_name . '[' . $views_option . '][]';
    $multiselect = '<div id="form" class="' . $views_option_class . ' multiselect" name="' . $multiselect_name . '">
        ';
    array_pop($views_options_item);
    foreach($views_options_item as $item) {
      if ((isset($options[$views_option]) && $options[$views_option] != '' && $options[$views_option] == $item['value']) ||
        (isset($options[$views_option]) && strpos($options[$views_option], ',') && strpos('X' . $options[$views_option], $item['value']))) {
        $multiselected = ' checked = "checked" ';
      } elseif (isset($item['selected']) && $item['selected']) {
        $multiselected = ' checked = "checked" ';
      } else {
        $multiselected = ' ';
      }
      if ($item['value'] == '-') {
        $multiselect .= '<label><input type="checkbox" class="' . $views_option_class . '" name="' . $multiselect_name . '" ' . $multiselected . 'value="">' . $item['display_name'] . '</label>
        ';
      } else {
        $multiselect .= '<label><input type="checkbox" class="' . $views_option_class . '" name="' . $multiselect_name . '" ' . $multiselected . 'value="' . $item['value'] . '">' . $item['display_name'] . '</label>
        ';
      }
    }
    $multiselect .= '</div>';
  }
  return array('label' => $label, 'item' => $multiselect);
  //return generate_select($views_options_item, $views_option, $request, true);
}

/************************************************************
  generate_options
  Outputs a series of options for the view
  @param $view REQUIRED id of the view
*************************************************************/
function generate_options($options = null, $request = null) {
  $menu = array();

  foreach($options as $option_key => $option) {
    if (isset($option['type']) && $option['type'] && $option['type'] != '') {
      $type = $option['type'];
    } else {
      $type = 'select';
    }
    $generation_function = 'generate_' . $type;
    $menu[$option_key] = $generation_function($option, $option_key, $request);
  }
  
  return $menu;
}

function generate_radio($views_options_item, $views_option, $options = null) {}

function generate_select($views_options_item, $views_option, $request, $multiple = false) {  
  if($multiple) {
    $multiple = 'multiple="multiple" size=10';
  }
  $options = array();
  for($i = 2; $i < count($request); $i += 2) {
    $options[$request[$i]] = $request[($i + 1)];
  }
  $select = array();
  if(count($views_options_item) > 1) {
    if (isset($views_options_item['placement']) && $views_options_item['placement'] != '') {
      $field_name = $views_options_item['placement'];
      unset($views_options_item['placement']);
    } else {
      $field_name = 'field';
    }
    if (isset($views_options_item['type']) && $views_options_item['type'] != '') {
      unset($views_options_item['type']);
    }
    if (isset($views_options_item['label']) && $views_options_item['label'] != '') {
      $label = '<label class="' . $views_option  . '">' . $views_options_item['label'] . '</label>';
      unset($views_options_item['label']);
    } else {
      $label = null;
    }
    if($multiple) {
      $select[] = '<select id="form" class="' . $views_option . ' multiselect" name="' . $field_name . '[' . $views_option . '][]" ' . $multiple . '>';
    } else {
      $select[] = '<select id="form" class="' . $views_option . '" name="' . $field_name . '[' . $views_option . ']">';
    }
    if(isset($views_options_item['element_name']) && $views_options_item['element_name'] != '' && !$multiple) {
      $select[] = '  <option value="">' . array_pop($views_options_item) . '</option>';
    } elseif ($multiple) {
      array_pop($views_options_item);
    }
    $select_has_been_used_already = false;
    foreach($views_options_item as $key => $item) {
      if ($item['value'] == '-') {
        $item['value'] = '';
      }

      if (isset($item['class']) && $item['class'] != '' && is_array($item)) {
        $class = ' class="' . $item['class'] . '"';
        unset($item['class']);
      } else {
        $class = '';
      }
      if (((isset($options[$views_option]) && $options[$views_option] != '' && $options[$views_option] == $item['value']) ||
        (isset($options[$views_option]) && strpos($options[$views_option], ',') && strpos('X' . $options[$views_option], $item['value'])) ||
        (isset($item['selected']) && $item['selected']) ||
        (chart_info('type') == 'all' && (($views_option == 'start_year' && $item['value'] == chart_info('start_year')) || ($views_option == 'end_year' && $item['value'] == END_YEAR))) ||
        (chart_info('type') == 'by_state' && $views_option == 'year' && $item['value'] == END_YEAR)) &&
        (!$select_has_been_used_already)) {
          $select[] = '<option selected value="' . $item['value'] . '"' . $class . '>' . $item['display_name'] . '</option>';
          $select_has_been_used_already = true;
      } else {
        $select[] = '<option value="' . $item['value'] . '"' . $class . '>' . $item['display_name'] . '</option>';
      }  
    }
    $select[] = '</select>';
  }
  return array('label' => $label, 'item' => implode('', $select));
}

function generate_submit($views_options_item, $views_option, $request = null) {
  return array('item' => '<input type="submit" value="' . $views_options_item['display_name'] . '">');
}


//----------------------------------------- INFO-PARSING FUNCTIONS ----------------------------------------//
function is_advanced() {
  return in_array('advanced', $GLOBALS['request']);
}

function is_home() {
  return (isset($GLOBALS['request'][0]) && !(isset($GLOBALS['page_override']) && $GLOBALS['page_override']));
}

function process_vars($get) {
  $query_array = array();
  
  foreach($get as $name => $data) {
    if (is_array($data)) {
      foreach($data as $key => $value) {
        if(!isset($value) || $value == '') {
          unset($get[$name][$key]);
        }
      }
    }
  }
}

function splice_controls($array) {
  $control_array = array();
  if(in_array('type', $array)) {
    $control_array['type'] = $array[array_search('type', $array) + 1];
    $control_array['select'] = array_slice($array, 0, array_search('type', $array));
    if (in_array('sort', $array) && $array[count($array) - 1] != 'sort') {
      $control_array['conditions'] = array_slice($array, (array_search('type', $array) + 2), count($array) - count($control_array['select']) - 4);
      $control_array['order'] = $array[count($array) - 1];
    } else {
      if ($control_array['type'] == 'all') {
        $control_array['order'] = 1;
      }
      $control_array['conditions'] = array_slice($array, (array_search('type', $array) + 2));
    }
  }
  return $control_array;
}


//---------------------------------------- PAGE-RENDERING FUNCTIONS ---------------------------------------//

function get_page_data($special_return = false) {
        $sections = array();
        if(isset($GLOBALS['sections'])) {
    $sections = $GLOBALS['sections'];
        }
  
  if(!$special_return) {
    return $sections;
  } else {
    $request = $GLOBALS['request'];

    if(empty($request) || $request[0] == 'pages') {
      return false;
    } else {
      $this_model = $request[0];
    }
  
    $links = array();
    
    foreach($sections as $link) {
      unset($link['about_section']);
      $links = array_merge($links, $link);
    }
          
    if($special_return == 'model') {
      for($i = 0; $i < count($links) && !is_numeric($this_model); $i++) {
        if (in_array($this_model, $links[$i]['match'])) {
          $this_model = $i;
        }
      }
      return $links[$this_model];
    } else {
      return $links;    
    }
  }
}

function advanced_link() { //disabled
/*  $request = $GLOBALS['request'];
  $function_name = $request[0] . '::advanced';

  if ($request[0] != 'pages' && is_callable($function_name)) {
    $link = '<div class="advanced-link">
      <a href="/' . $request[0] . '/';
    if (!is_advanced()) {
      $link .= 'advanced">Switch to Advanced';
    } else {
      $link .= '">Switch to Standard';  
    }
    $link .= ' View</a>
    </div>';
    return $link;
  } else {*/
    return '';
  //}
}

function render_about_graph() {
  if (!isset($GLOBALS['request']) && (!isset($request) || !$request)) {
    $request = $GLOBALS['request'];
  }
  if(!empty($request) && $request[0] != 'pages') {
    echo '<div id="graph-info">
      <h5>About This Graph</h5> 
      ' . render_sources(true) . '
    </div>';
  }
}

function render_about_section($request = false) {
  $sections = get_page_data();
  
  $links = array();
  
  if (!$request) {
    $request = $GLOBALS['request'];
  }
  
  foreach($sections as $title => $section) {
    foreach ($section as $item) {
      if(isset($item['match']) && is_array($item['match']) && in_array($request[0], $item['match'])) {
        echo '<div id="about-info">
          <h5>About ' . $title . '</h5>  
          ' . $section['about_section']. '
        </div>';
      }
    }
  }
}

function render_breadcrumb($request = false) {  
  $links = get_page_data();

  if (!$request) {
    $request = $GLOBALS['request'];
  }
  if(empty($request) || $request[0] == 'pages') {
    return false;
  } else {
    $this_model = $request[0];
  }
  
  foreach($links as $section=> $link) {
    unset($link['about_section']);
    for($i = 0; $i < count($link) && !is_numeric($this_model); $i++) {
      if (in_array($this_model, $link[$i]['match'])) {
        $set ='     <!-- start breadcrumb -->
              <div class="breadcrumb" id="">
                &nbsp;<br>' . $section . ' &raquo; <a href="/' . $this_model . '">' . $link[$i]['text'] . '</a>
                </div>
              <!-- end breadcrumb -->';
         $i = count($link) + 1;
      }
    }
  }
   
   if(isset($set)) {
    echo $set;
  }
}

function render_chart($request = false) {
  if (!$request) {
    $request = $GLOBALS['request'];
  }
  if(!empty($request) && $request[0] == 'pages') {
    return false;
  }
  if(isset($GLOBALS['page_override']) && $GLOBALS['page_override']) {
    require_once($GLOBALS['override_page']);
  } elseif (isset($request[1]) && $request[1] != '') {
    $chart = /*'<script src="/layouts/js/highcharts.js"></script>
          <script src="/layouts/js/modules/exporting.js"></script>*/
          '<script src="http://code.highcharts.com/highcharts.js"></script>
          <script src="http://code.highcharts.com/modules/exporting.js"></script>
          <script type="text/Javascript" language="Javascript">
      ' . $GLOBALS['chart'] . '
          </script>
          <div id="container" style="background-color: #F6F7F7; min-width: 960px; height: 425px; margin: 0px auto; float: left;"></div>
          ';
  } else {
    include_once('pages/home.edpc');
  }
  echo $chart;
}

function render_footnote() {
  /*$request = $GLOBALS['request'];
  if(isset($request[0]) && $request[0] != '' && $request[0] != 'favicon.ico') {    
    include_once('views/footnotes.edpc');
  }*/
}

function render_form($views_option = null, $request = null) {
  if (!$request) {
    $request = $GLOBALS['request'];
  }
  if($request[0] == 'pages') {
    return false;
  }
  if(isset($views_option) && $views_option != '' && is_array($views_option) && isset($request) && $request != '' && is_array($request)) {
    $options = generate_options($views_option, $request);
  } else {
    return null;
  }

  $html = '<div class="menu ' . $request[1] . '">
    <div class="inner-menu">
  ';
  if(isset($options) && $options != '' && is_array($options)) {
    $current_placement = '';
    $first = true;
    foreach ($options as $title => $option) {
      if ($current_placement != $views_option[$title]['placement']) {
        if (!$first) {
          $html .= '</div>
          ';
        } else {
          $first = false;
        }
        $html .= '<div class="' . $views_option[$title]['placement'] . '">
        ';
        $current_placement = $views_option[$title]['placement'];
      }
      $html .= '<div class="' . $title . '">
      ';
      if(isset($option['label']) && $option['label'] != '') {
        $html .= $option['label'] . '
        ';
      }
      $html .= $option['item'] . '
      ';
      $html .= '</div>
      ';
    }
    $html .= '</div>
    ';
  }
  $html .= '<div class="clear"></div>
  </div>';
  return $html;
}

function render_links($request = false) {
  $sections = get_page_data();
  $links = array();

  if (!$request) {
    $request = $GLOBALS['request'];
  }
  if(empty($request) || $request[0] == 'pages' || (isset($GLOBALS['page_override']) && $GLOBALS['page_override'])) {
    return false;
  } else {
    $this_model = $request[0];
  }
  
  foreach($sections as $section_name => $section) {
    $section[0]['text'] = $section_name;
    unset($section['about_section']);
    $match = array();
    for($i = 0; $i < count($section); $i++) {
      $match = array_merge($match, $section[$i]['match']);
    }
    $section[0]['match'] = $match;
    $links[] = $section[0];
  }

  for($i = 0; $i < count($links); $i++) {
    if(in_array($this_model, $links[$i]['match'])) {
      $this_model = $i;
      $i = count($links) + 1;
    }
  } 

  $prev_model = $this_model - 1;
  if($prev_model < 0) {
    $prev_model = count($links) - 1;
  }
  
  $next_model = $this_model + 1;
  if($next_model >= count($links)) {
    $next_model = 0;
  }

        $set = '';
        if(isset($links[$prev_model])) {
    $set ='     <!-- start set -->
        <div class="button-set" id="">
              <div class="prev-button">
                <span class="arrows"><a href="/' . $links[$prev_model]['link'] . '">&laquo;</a></span>
                <span class="over-button">Previous</span>
                <span class="button-text"><a href="/' .$links[$prev_model]['link']  . '">' . $links[$prev_model]['text'] . '</a></span>
              </div>
              <div class="next-button">
                <span class="over-button">Next</span>
                <span class="button-text"><a href="/' . $links[$next_model]['link'] . '">' . $links[$next_model]['text'] . '</a></span>
                <span class="arrows"><a href="/' . $links[$next_model]['link'] . '">&raquo;</a></span>
              </div>  
              <div class="clear"></div>
          </div></div>
        <!-- end set -->';
        }   
  echo $set;
}

function render_options($request = false) {
  if (!$request) {
    $request = $GLOBALS['request'];
  }
  if(!empty($request) && $request[0] == 'pages') {
    return false;
  }
  if(isset($request[0]) && $request[0] != '') {
    if (!isset($request[1])) {
      $request[1] = '';
    } 
    if (!isset($GLOBALS['views_option'])) {
      set('views_option', '');
    }
    $chart = '<form action="/config/parse.php" method="post">
          <input type="hidden" name="traceback" class="traceback" value="' . $request[0] . '/' . $request[1] . '">';
         $chart .= advanced_link();
      $chart .= render_form($GLOBALS['views_option'], $request);
    $chart .= '</form>
      <script src="/layouts/js/options.js"></script>
      <script src="/layouts/js/' . $request[0] . '.js"></script>
    ';
    echo $chart;
  }
}

function render_page() {
  $request = $GLOBALS['request'];
  if(isset($request[0]) && $request[0] != '' && $request[0] != 'favicon.ico' && $request[0] != 'layouts') {
    if(isset($request[1]) && $request[1] != '' && $request[1] != 'js') {
      $view = $request[1];
    } else {
      $view = 'index';
    }
    if($request[0] != 'pages') {
      $type = 'views/';
    } else {
      $type = '';
    }
    include_once($type . $request[0] . '/' . $view . '.edpc');
    
  } elseif(!(isset($GLOBALS['page_override']) && $GLOBALS['page_override'])) {
    include_once(HOMEPAGE);
  }
}

function render_sources($about = false) {
  $request = $GLOBALS['request'];
  $abouts = get_page_data('model');
  if(chart_info('data') !== false) {
    $metric = chart_info('data');
  } elseif(chart_info('statistic') !== false) {
    $metric = chart_info('statistic');
  } elseif(chart_info('rate') !== false) {
    $metric = chart_info('rate');
  }
        
        if(isset($request[0]) && ($request[0] == 'rank')) {
    $item = 'sources';
    $pre = 'Source: Compare50.org';
        } elseif(!$about) {
    $item = 'sources';
    $pre = 'COMPARE50.ORG.  Data Source: ';
  } else {
    $item = 'about';
    $pre = '';
  }

  return $pre . $abouts['graphs'][$metric][$item];
}

function render_super_important_thing() {
  echo '
  <!-- While this site may be full of numbers, I always count on Xavier.-->
  ';
}

function set_up($request = null) {
  if(isset($request[0]) && $request[0] != '' && $request[0] != 'favicon.ico') {
    if(isset($request[1]) && $request[1] != '') {
      $function = $request[1];
    } else {
      $function = 'index';
    }
    $type = $request[0];
    $model_name = $type . 'Model';
    $function_name = $type . '::' . $function;
    
    require_once("config/sections.php");
    require_once("models/model.php");
    require_once("models/model2.php");
    require_once("controllers/controller.php");
    if($type != 'layouts') {
      require_once('controllers/' . $type . '.php');
      require_once('models/' . $type . 'Model.php');
    }

    if ((!is_callable($function_name) && $type != 'pages') || 
      ($type != 'pages' && !file_exists('views/' . $type . '/' . $function . '.edpc')) ||
      ($type == 'pages' && !file_exists('pages/' . $function . '.edpc'))) {
        return false;
    }
    if (is_callable($function_name)) {
      $model = new $model_name();  
      $results = @$type::$function(splice_controls(array_slice($request, 2)));;
    }
    
    if($type != 'pages') {
      if ((!isset($results) || !$results) && isset($GLOBALS['default_request'])) {
        $request = $GLOBALS['request'] = $GLOBALS['default_request'];
        $model = new $model_name();
        $results = @$type::$function(splice_controls(array_slice($request, 2)));
      }
    } else {
      set('view_name', ucfirst($function));
    }
    if (!isset($GLOBALS['default_request']) && isset($GLOBALS['page_override'])) {
      $filename = "pages/" . $type . ".php";
      set('override_page', $filename);
    } else {
      require_once("config/chart.php");
    }
  } else {
    set('view_name', 'Home');
  }

  return true;
}

/*function variant_phrase($phrase = null, $special_plural = false) {
  if(!isset($phrase)) {
    return false;
  } else {
    if(chart_info('type') == 'all') {
      if($special_plural !== false) {
        $phrase = $special_plural;
      } else {
        $phrase .= 's';
      }
      return 'change in ' . $phrase . ' over time';
    } else {
      return $phrase;
    }
  }
}*/

?>
