<?php

$url = '';

foreach($_POST as $key => $value) {
  if(isset($value) && $value != '') {
    if(is_array($value)) {
      foreach($value as $value_key => $value_value) {
        if (isset($value_value) && $value_value != '')
          $url .= '/' . $value_key . '/';
          if (is_array($value_value)) {
            foreach($value_value as $omfg) {
              $url .= $omfg . ',';
            }
            $url = substr($url, 0, -1);
          } else {
            $url .= $value_value;
          }
      }
    } else {
      $url .= '/' . $value;
    }
  }
}

header('Location: ' . $url);

?>
