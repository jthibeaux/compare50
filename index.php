<?php 

include_once("config/config.php");
include_once("config/functions.php");

$request = explode('/', substr($_SERVER["REQUEST_URI"], 1));
$count = count($request);
for ($i = 0; $i < $count; $i++) {
	if (empty($request[$i])) {
		unset($request[$i]);
	}
}
set_up($request);

include_once('layouts/' . LAYOUT . '.edpc');

?>