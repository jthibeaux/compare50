<script>
  window.onload = function() {
    $( "ul.droptrue#choices" ).sortable({
      connectWith: "ul",
      dropOnEmpty: true
    });
    $("ul.droptrue#priorities").sortable({
        connectWith: "ul",
        dropOnEmpty: true,
        //receive: This event is triggered when a
        //connected sortable list has received an item from another list.
        receive: function(event, ui) {
            // so if > 10
            if ($(this).children().length > 10) {
                //ui.sender: will cancel the change.
                //Useful in the "receive" callback.
                $(ui.sender).sortable("cancel");
            }
        }
    }).disableSelection();

    $( "#choices, #priorities" ).disableSelection();
  }
</script>

<?php
require_once('models/rankModel.php');
@rankModel::do_get_options();
@rankModel::do_get_details();
?>

<div id="container" style="background-color: #F6F7F7; min-width: 960px; height: auto; margin: 0px auto; float: left;">

<!-- start breadcrumb -->
<div class="breadcrumb" id="">
  &nbsp;<br>Rank &raquo; <a href="/rank">SELECTION</a>
</div>
<!-- end breadcrumb -->

Select up to ten different indicators of economic performance by dragging it to the right and arranging it based on what you think is most important.

<form id="priority-form" method="post" action="/config/parse.php">
  <div class="left-list">
    <ul id="choices" class="droptrue">
    <?php 
      foreach($GLOBALS['views_option']['data'] as $data) {
        if(isset($data['value']) && isset($GLOBALS['views_option'][$data['value']])) {
          foreach($GLOBALS['views_option'][$data['value']] as $value) {
            if(is_array($value)) {
              echo '<li class="ui-state-default" id="' . $data['value'] . '+' . $value['value'] . '">' . $data['display_name'] . ' - ' . $value['display_name'] . '</li>';
            }
          }

        } else {

        }
        
      }
      ?>
    </ul>
  </div>
  <div class="right-list">
    <ul id="priorities" class="droptrue">
    </ul>
    <input type="hidden" name="traceback" class="traceback" value="rank/index">
    <input type="hidden" name="p1">
    <input type="hidden" name="p2">
    <input type="hidden" name="p3">
    <input type="hidden" name="p4">
    <input type="hidden" name="p5">
    <input type="hidden" name="p6">
    <input type="hidden" name="p7">
    <input type="hidden" name="p8">
    <input type="hidden" name="p9">
    <input type="hidden" name="p10">
    <div class="clear"></div>
    Weight:<br>
    <input type="radio" name="weight" value="" selected>
    Ranked
    <input type="radio" name="weight" value="unranked">
    Same
    <div class="right-list-button">
      <button id="submit" type="submit">Submit</button>
    </div>
  </div>

</form>

<script>
  var max_priorities = 10;

  function numberOff() {
    var start = 1;
    $('#priorities li').each(function() {
      $('input[name="p' + start + '"]').val($(this).attr('id'));
      start++;
      if(start == 10) {
        //break;
      }
    });
  }

  $('#priority-form').submit(function(e) {
    //e.preventDefault();
    numberOff();
  });
</script>

<br style="clear: both;" />

</div>
